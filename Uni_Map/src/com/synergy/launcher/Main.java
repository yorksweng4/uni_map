package com.synergy.launcher;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.synergy.slideshow.SlideShow;
import com.synergy.slideshow.SlideShowEvent;
import com.synergy.slideshow.parser.SynergyXMLReader;
import com.synergy.unimap.Alert;
import com.synergy.unimap.UniMap;

/**
 * This class provides a launcher for demonstration purposes.
 * 
 * From this interface the user can open a standalone slideshow from a local XML or a hosted one,
 * or access the UniMap
 *
 * @author Matt
 */
public class Main extends Application {

	private TextField address;
	private Stage ssStage;
	private Stage uniStage;

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(final Stage primaryStage) {

		//set up stages
		ssStage = new Stage();
		uniStage = new Stage();

		//name stages
		primaryStage.setTitle("Browser");
		uniStage.setTitle("UniMap App");

		//COMMENT
		
		//Another comment
		
		final VBox vBox = new VBox();
		HBox addressButtons = new HBox();

		// set up text field
		address = new TextField();
		address.setPromptText("Enter URL of XML");

		//button for file browser
		Button browseButton = new Button("Browse...");
		browseButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();

				//Set extension filter
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("xml files (*.xml)", "*.xml");
				fileChooser.getExtensionFilters().add(extFilter);

				//Show open file dialog
				openSlideShow(fileChooser.showOpenDialog(null).getAbsolutePath());

			}



		});
		
		// button for URL
		Button goButton = new Button("Go");
		goButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				if(address.getText() != null && !address.getText().isEmpty()){
					try {
						openSlideShow(new URL(address.getText()).toURI().toString());
					} catch (MalformedURLException e1) {
						e1.printStackTrace();
					} catch (URISyntaxException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		//button to open UniMap App
		Button unibtn = new Button();

		unibtn.setText("Open UniMap");
		unibtn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {


				//just playing around probably not the best way to do it
				UniMap map = new UniMap();

				try {
					//display in new stage
					map.start(uniStage);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		//display option buttons
		addressButtons.getChildren().addAll(browseButton, goButton);
		vBox.getChildren().addAll(address, addressButtons, unibtn);
		vBox.setSpacing(3.0);

		primaryStage.setScene(new Scene(vBox, 300, 100));
		primaryStage.show();
	}


	/**
	 * Opens a slideshow with the given file.
	 * 
	 * @param file the XML file to be parsed.
	 */
	private void openSlideShow(String file) {
		ssStage.setTitle("Slideshow: " + file);

		//show file location				
		SynergyXMLReader reader;

		try {
			reader = new SynergyXMLReader();
			reader.parseFile(file);

			// make contentPane and tab
			SlideShow alss = new SlideShow(reader.getPresentable());
			System.out.println(reader.getPresentable().toString());

			alss.setOnPause(new EventHandler<SlideShowEvent>() {

				@Override
				public void handle(SlideShowEvent e) {
					ssStage.close();

				}
			});

			//display in new stage
			ssStage.setScene(new Scene(alss, 800, 600));
			ssStage.show();
			alss.play();
		}catch (SAXParseException pe){
			Alert.displayError(pe.getMessage()+" @Line: "+pe.getLineNumber());
		} catch (SAXException e) {
			Alert.displayError(e.getMessage());
		}
	}
}
