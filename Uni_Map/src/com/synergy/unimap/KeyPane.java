package com.synergy.unimap;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * This Group contains the map key, providing a description of every icon used.
 * 
 * @author Mike, Ben
 *
 */
public class KeyPane extends Group{
	
	private Pane labelContainer;
	private Pane key;
	private Label hoverTitle;
	private ArrayList<Label> labelArray = new ArrayList<Label>(); 
    private ArrayList<String> imageArray = new ArrayList<String>();
    private String hostURL;
    private InputStream inURL = null;

	public KeyPane(){
		//Where are the icons hosted?
		hostURL = "http://benshouse.crabdance.com/xml/icons/";
		
		//Container pane for key
		labelContainer = new Pane();
		labelContainer.setTranslateX(-82);  //Set the label correctly relative to the key group
		labelContainer.setTranslateY(9);
		labelContainer.setMaxSize(40, 35);
		labelContainer.setStyle("-fx-background-color: rgba(0,0,0,0.35)");
		labelContainer.setVisible(true);
		
		//Label for hover over point
		hoverTitle = new Label("Key");
		hoverTitle.setFont(new Font(20));
		hoverTitle.setTextFill(javafx.scene.paint.Color.WHITE);
		hoverTitle.setVisible(true);
		
		//Pane for the actual Key
		key = new Pane();
		key.setVisible(false);
		key.setStyle("-fx-background-color: rgba(0,0,0,0.35)");
		key.setTranslateX(-181); //Set the Key correctly relative to the key group
		key.setTranslateY(9);
		
		//Read through "key" file hosted with the icons.
		//Populate the image and label arrays from information in the key file.
		URL hostedKey;
		try {
			hostedKey = new URL(hostURL + "key");
			BufferedReader in = new BufferedReader(new InputStreamReader(hostedKey.openStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null){
	        	
	        	//Split into icon URL and icon description
	        	String[] icon = inputLine.split("<-");
	        	
	        	//Take the String before "<-" to be the URL on the icon, trim any spaces
	        	imageArray.add(icon[0].trim());
	        	
	        	//It is possible an icon description is forgotten
	        	if(icon.length == 2){
	        		//Take String after "<-" to be icon description
	        		labelArray.add(new Label(icon[1].trim()));
	        	} else {
	        		labelArray.add(new Label(" "));
	        	}
	        }
	        in.close();
		} catch (MalformedURLException e2) {
			e2.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	        
		//the following for loop fills the keyPopUp pane with the icons and titles, 
		//they are nicely spaced within the loop
		for(int i = 0; i<imageArray.size() ; i++){
			//image imported, sized and added to the pane
			try {
				inURL = new BufferedInputStream(new URL(imageArray.get(i)).openStream());
				Image keyImage = new Image(inURL);
				ImageView keyImageView = new ImageView(keyImage);
				keyImageView.setLayoutY(i*40);
				keyImageView.setLayoutX(100);;
				keyImageView.setFitHeight(30);
				keyImageView.setFitWidth(30);
				labelArray.get(i).setLayoutY((i*40)+10);
				labelArray.get(i).setTextFill(javafx.scene.paint.Color.WHITE);
				labelArray.get(i).setFont(Font.font("Verdana", FontWeight.BOLD, 12));
				//image and title added to the pane
				key.getChildren().addAll(keyImageView, labelArray.get(i));
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}			
		}
		
		//if the user hovers over the key title, the key pane appears
		labelContainer.setOnMouseEntered(new EventHandler<MouseEvent>(){
		    @Override public void handle(MouseEvent e) {
		    	key.setVisible(true);
		    	//key.setMouseTransparent(false);
		    	labelContainer.setVisible(false);
		    }	
		});
		 
		//if the user moves the mouse out of the key pane then the key pane disseapears
		key.setOnMouseExited(new EventHandler<MouseEvent>(){
		    @Override public void handle(MouseEvent e) {
		    	key.setVisible(false);
		    	//key.setMouseTransparent(true);
		    	labelContainer.setVisible(true);
		    }	
		});

		labelContainer.getChildren().add(hoverTitle);
		this.getChildren().addAll(labelContainer, key);
	}
	
}
