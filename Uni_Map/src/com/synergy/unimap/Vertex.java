package com.synergy.unimap;
/**
 * Class Vertex. 
 * @Author Josh 
 */
public class Vertex implements Comparable<Vertex> {
	private final String name;
	private Edge[] adjacencies;
	private double minDistance = Double.POSITIVE_INFINITY;
	private Vertex previous;

	/**
	 * @param argName
	 */
	public Vertex(String argName){
		name = argName; 
	}

	/**
	 * @return node 
	 */
	public String toString(){
		return name; 
	}

	/**
	 *  Vertex comparator
	 *  @param other 
	 *  @return minDistance 
	 */
	public int compareTo(Vertex other) {
		return Double.compare(getMinDistance(), other.getMinDistance());
	}
	/**
	 * @return the minDistance
	 */
	public double getMinDistance() {
		return minDistance;
	}
	/**
	 * @param minDistance the minDistance to set
	 */
	public void setMinDistance(double minDistance) {
		this.minDistance = minDistance;
	}
	/**
	 * @return the previous
	 */
	public Vertex getPrevious() {
		return previous;
	}
	/**
	 * @param previous the previous to set
	 */
	public void setPrevious(Vertex previous) {
		this.previous = previous;
	}
	/**
	 * @return the adjacencies
	 */
	public Edge[] getAdjacencies() {
		return adjacencies;
	}
	/**
	 * @param adjacencies the adjacencies to set
	 */
	public void setAdjacencies(Edge[] adjacencies) {
		this.adjacencies = adjacencies;
	}
}