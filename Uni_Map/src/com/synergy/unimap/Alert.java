package com.synergy.unimap;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBuilder;
import javafx.util.Duration;

/**
 * 
 * This class provides a globally accessible way for the application to produce warnings.
 * This is intended as a way to handle exceptions by warning the user that something went wrong, 
 * and providing information as to what the error might be.
 * 
 * @author Ben
 *
 */
public abstract class Alert {
	
	private static Pane errorPane;
	private static StackPane messagePane;
	private static Text errorText, messageText;
	
	/**
	 * This static method can be called from anywhere within the program.
	 * It displays an error message over the rest of the program, with a
	 * close button.
	 * 
	 * @param error the error message to be displayed.
	 */
	public static void displayError(String error){
		errorText.setText(error);
		errorPane.setOpacity(1);
		errorPane.setVisible(true);
	}
	
	/**
	 * Returns the instantiated static Pane which displays the error messages.
	 * This should be overlaid on the rest of the application GUI. 
	 * 
	 * @return the error pane.
	 */
	public static Pane getPane(){
		// create the container pane
		errorPane = new Pane();
		errorPane.setPrefSize(800, 600);
		errorPane.setStyle("-fx-background-color: #"+"000000");
		errorPane.setVisible(false);
		
		// need a pane for the message as well
		messagePane = new StackPane();
		messagePane.setPrefSize(800, 200);
		messagePane.setLayoutY(200);
		
		messageText = TextBuilder.create()
				.fill(Color.web("FFFFFF"))
				.underline(true)
				.font(Font.font("Arial", FontPosture.REGULAR,30.0))
				.build();
		messageText.setText("An error has occurred:");
		
		// create the static text on it
		errorText = TextBuilder.create()
				.fill(Color.web("FFFFFF"))
				.underline(false)
				.font(Font.font("Arial", FontPosture.REGULAR,20.0))
				.build();
		errorText.setWrappingWidth(800);
		errorText.setTextAlignment(TextAlignment.CENTER);
		
		// set everything up
		messagePane.getChildren().addAll(messageText, errorText);
		StackPane.setAlignment(messageText, Pos.TOP_CENTER);
		StackPane.setAlignment(errorText, Pos.CENTER);
		
		errorPane.getChildren().add(messagePane);
		// exit button
		final Button exitPane;
		exitPane = new Button("X");
		exitPane.setStyle("-fx-base: red;" + "-fx-font-weight: bold;");
		exitPane.setLayoutX(errorPane.getPrefWidth() - 40);
		exitPane.setLayoutY(5);
		errorPane.getChildren().add(exitPane);
		
		// action listener, pause() on press
		exitPane.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				//If there was a loading screen before the problem, close it.
				Loading.hideBusy();
				Loading.hideProgress();
				// fade out
				FadeTransition ft = new FadeTransition(Duration.millis(200), errorPane);
				ft.setFromValue(1.0);
				ft.setToValue(0.0);

				// invisible when finished
				ft.setOnFinished(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent arg0) {
						errorPane.setVisible(false);	
					}
				});

				// go!
				ft.play();
			}

		});
		
		return errorPane;
	}

}
