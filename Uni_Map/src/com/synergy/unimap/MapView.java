/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */

package com.synergy.unimap;

import java.net.URL;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

import com.synergy.unimap.POI.HeadPOI;

/**
 * A class for creating the map window using html and javascript.
 * 
 * @author Mike Holmes, Matt Dale, Andy Cox, Kat Young
 * 
 */
public class MapView {

	private WebView webView = new WebView();
	private WebEngine webEngine = webView.getEngine();
	private List<HeadPOI> headPOIs;

	private UniMap parent;

	/**
	 * This constructor creates the webView itself.
	 *  
	 * @param list a list of HeadPOI which should be added to the webView map.
	 * @param ref a reference to UniMap, for inversion of control.
	 */
	public MapView(List<HeadPOI> list, UniMap ref) {
		this.headPOIs = list;
		parent = ref;

		// creating arrays to hold the attribute values from the parser
		// separately
		final Double[][] latLng = new Double[headPOIs.size()][2];
		final String[] poiNames = new String[headPOIs.size()];
		final String[] poiIcons = new String[headPOIs.size()];
		final String[] poiPhotos = new String[headPOIs.size()];


		// populating the arrays to use in the javascript
		for (int i = 0; i < headPOIs.size(); i++) {
			// only ever going to be 2 wide - lat and lng
			latLng[i][0] = headPOIs.get(i).getLat();
			latLng[i][1] = headPOIs.get(i).getLng();
			poiNames[i] = headPOIs.get(i).getName();
			poiIcons[i] = headPOIs.get(i).getIconFilename();
			poiPhotos[i] = headPOIs.get(i).getPhoto();
		}

		// load html
		final URL mapUrl = getClass().getResource("googlemap.html");
		webEngine.load(mapUrl.toExternalForm());
		webView.setMaxSize(800, 600);

		// places all icons on the map, including listeners
		webEngine.getLoadWorker().stateProperty()
		.addListener(new ChangeListener<State>() {
			@Override
			public void changed(ObservableValue<? extends State> ov,
					State oldState, State newState) {
				if (newState == State.SUCCEEDED) {
					// for as many HeadPOI are there are in the array,
					// call placeIcons
					for (int i = 0; i < headPOIs.size(); i++) {
						JSObject window = (JSObject) webEngine
								.executeScript("window");
						window.setMember("app", new JavaApplication());
						webView.getEngine().executeScript(
								"placeIcons(" + latLng[i][0] + ","
										+ latLng[i][1] + ",'"
										+ poiNames[i] + "','"
										+ poiIcons[i] + "','"
										+  poiPhotos[i]  + "')");

					}
				}
			}
		});
		
	}


	/**
	 * Returns webView
	 */
	public WebView returnBrowser(){
		return webView;

	}


	/**
	 * Called when the go button is pressed. This runs the javascript function
	 * to display route
	 * 
	 * @param startLat
	 * @param startLng
	 * @param endLat
	 * @param endLng
	 */
	public void runRoute(Double startLat, Double startLng, Double endLat, Double endLng) {
		
		webView.getEngine().executeScript( "directionsFunction(" + startLat + "," + startLng + "," + endLat + "," + endLng + ")");
		
	}

	/**
	 * Executes javascript function to centre the map on the relevant Head POI
	 * 
	 * @param lat, lng
	 */
	public void centerMap(Double lat, Double lng, int zoomLevel) {
		webView.getEngine().executeScript(
				"centerMap(" + lat + "," + lng + "," + zoomLevel + ")");
	}


	/**
	 * Executes javascript function to display distance on the console
	 * 
	 * @return distance in meters
	 */
	public Double getDistance() {
		Double distance = Double.parseDouble((String) webView.getEngine()
				.executeScript("getDistance()"));
		return distance;
	}


	public void removeRoute(){
		webView.getEngine().executeScript(
				"removeRoute()");
	}


	/**
	 * JavaScript interface object if you want to call any java methods from
	 * javascript place them here
	 * 
	 */
	public class JavaApplication {

		/**
		 * Writes map coordinates to console
		 * @param x
		 * @param y
		 */
		public void callFromJavascript(double x, double y) {
			// labelFromJavascript.setText("Click from Javascript: " + msg);
			System.out.println(x + " , " + y);

		}
		/**
		 * This method is called when an icon on the map is pressed, it in-turn starts the slideshow
		 */
		public void displayInfo() {
			//Executes javascript function to get poi selected
			String poiInfo = (String) webView.getEngine().executeScript("getPoi()");

			System.out.println("start " + poiInfo +" slideshow");

			//These two create, build, show and start the slideshow
			parent.displayPoiSlideshow(poiInfo);

			//ContentPane.runSlide();

			System.out.println("launching slideshow");
		}

		/**
		 * Similar to displayInfo method but calls a panoramic slideshow
		 */
		public void displayPano() {

			//Executes javascript function to get poi selected
			String poiPano = (String) webView.getEngine().executeScript("getPoi()"); 

			//These two create, build, show and start the slideshow
			parent.displayPanoSlideshow(poiPano);           
			//ContentPane.runSlide();

		}
	}

}
