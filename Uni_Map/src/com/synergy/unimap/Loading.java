package com.synergy.unimap;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * 
 * This class provides a globally accessible way for the application to display loading progress.
 * Any time-consuming, blocking operations should be threaded and this should be displayed in the meantime.
 * It can display a progress bar, or a simple spinning icon indicating that something is being done.
 * 
 * @author Ben
 *
 */
public abstract class Loading {

	private static StackPane barPane;
	private static GridPane loadingGrid;
	private static ProgressBar pb;
	private static ProgressIndicator pin;
	private static double progress;
	private static Text message;
	private static boolean kill = false;

	/**
	 * This static method can be called from anywhere within the program.
	 * It displays the loading screen over the rest of the application.
	 * 
	 */
	public static void showProgress() {
		kill = false;

		FadeTransition ft = new FadeTransition(Duration.millis(300),
				loadingGrid);
		ft.setFromValue(0.0);
		ft.setToValue(1.0);

		// visible when finished
		ft.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				pb.setVisible(true);
				loadingGrid.setVisible(true);
			}
		});

		// go!
		ft.play();
	}

	/**
	 * This method hides the progress bar - should be called when the time-consuming operation is finished.
	 */
	public static void hideProgress() {
		// fade out
		FadeTransition ft = new FadeTransition(Duration.millis(350),
				loadingGrid);
		ft.setFromValue(1.0);
		ft.setToValue(0.0);

		// invisible when finished
		ft.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				loadingGrid.setVisible(false);
				pb.setVisible(false);
				clearMessage();
			}
		});

		// go!
		ft.play();

		pb.setProgress(0.0);

	}

	/**
	 * This method changes the progress displayed by the progress bar. 
	 * 
	 * @param i a number between 0 and 1.
	 */
	public static void updateProgress(double i) {

		progress = i;

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				pb.setProgress(progress);
			}
		});

	}

	/**
	 * This method allows a custom message to be shown on the Loading screen.
	 * 
	 * @param m the message to be displayed.
	 */
	public static void showMessage(String m) {
		message.setText(m);
	}

	/**
	 * Clears the message displayed.
	 */
	public static void clearMessage() {
		message.setText("");
	}

	/**
	 * This method displayes the busy loading screen; progress can't be updated as it shows a loading
	 * spinner instead of a loading bar.
	 */
	public static void showBusy() {
		kill = false;

		FadeTransition ft = new FadeTransition(Duration.millis(300),
				loadingGrid);
		ft.setFromValue(0.0);
		ft.setToValue(1.0);

		// visible when finished
		ft.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				pin.setVisible(true);
				loadingGrid.setVisible(true);
			}
		});

		// go!
		ft.play();

	}

	/**
	 * Hides the busy loading screen, should be called when loading is completed.
	 */
	public static void hideBusy() {
		// fade out
		FadeTransition ft = new FadeTransition(Duration.millis(350),
				loadingGrid);
		ft.setFromValue(1.0);
		ft.setToValue(0.0);

		// invisible when finished
		ft.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				loadingGrid.setVisible(false);
				pin.setVisible(false);
				clearMessage();
			}
		});

		// go!
		ft.play();

		pb.setProgress(0.0);
	}

	/**
	 * Return the loading pane itself to be overlaid on the rest of the application.
	 * 
	 * @return the static loading pane.
	 */
	public static GridPane getPane() {

		// instantiate things
		loadingGrid = new GridPane();
		loadingGrid.setPrefSize(800, 600);
		loadingGrid.setStyle("-fx-background-color: rgba(192, 192, 192, 0.9);");
		loadingGrid.setVisible(false);
		
		loadingGrid.getColumnConstraints().add(new ColumnConstraints(100));
		loadingGrid.getColumnConstraints().add(new ColumnConstraints(600));
		loadingGrid.getColumnConstraints().add(new ColumnConstraints(100));

		barPane = new StackPane();
		barPane.setVisible(true);

		pb = new ProgressBar(0);
		pb.setMinSize(400.0, 15.0);
		pb.setMaxSize(400.0, 15.0);
		StackPane.setAlignment(pb, Pos.CENTER);
		pb.setVisible(false);

		pin = new ProgressIndicator(-1);
		pin.setMaxSize(40.0, 40.0);
		StackPane.setAlignment(pin, Pos.CENTER);
		pin.setVisible(false);

		barPane.getChildren().addAll(pin, pb);

		Text t = new Text("Loading");
		t.setFont(new Font(40));

		message = new Text();
		message.setFont(new Font(15));
		
		// exit button
		final Button exitLoading;
		exitLoading = new Button("X");
		exitLoading.setStyle("-fx-base: red;" + "-fx-font-weight: bold;");

		// action listener, pause() on press
		exitLoading.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				//If there was a loading screen before the problem, close it.
				Loading.hideBusy();
				Loading.hideProgress();
				// fade out
				FadeTransition ft = new FadeTransition(Duration.millis(200), loadingGrid);
				ft.setFromValue(1.0);
				ft.setToValue(0.0);
				// invisible when finished
				ft.setOnFinished(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent arg0) {
						kill = true;
						loadingGrid.setVisible(false);	
					}
				});
				// go!
				ft.play();
			}
		});

		// position things
		loadingGrid.add(t, 1, 1);
		GridPane.setHalignment(t, HPos.CENTER);
		GridPane.setValignment(t, VPos.CENTER);
		loadingGrid.add(message, 1, 2);
		GridPane.setHalignment(message, HPos.CENTER);
		GridPane.setValignment(message, VPos.CENTER);
		loadingGrid.add(barPane, 1, 3);
		GridPane.setValignment(barPane, VPos.CENTER);
		loadingGrid.add(exitLoading, 2, 0);
		GridPane.setHalignment(exitLoading, HPos.RIGHT);
		GridPane.setValignment(exitLoading, VPos.TOP);

		loadingGrid.getRowConstraints().add(new RowConstraints(200));
		
		kill = false;
		
		return loadingGrid;
	}
	
	/**
	 * This becomes true when the exit button is pressed, and can be used to interrupt time-consuming operations.
	 * 
	 * @return the value of kill.  
	 */
	public static boolean getKillThread(){
		return kill;
	}

}
