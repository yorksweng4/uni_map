package com.synergy.unimap;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.synergy.slideshow.SlideShow;
import com.synergy.slideshow.parser.SynergyXMLReader;
import com.synergy.unimap.POI.POIInfoPane;
import com.synergy.unimap.POI.POIXMLReader;
import com.synergy.unimap.POI.RouteFinderPane;

/**
 * Main application class.
 * 
 * @author Eddy, Matt, Mike, Ben
 * 
 */
public class UniMap extends Application {

	private SlideShow alss;
	private PanoViewer pv;
	private TabPane tp;
	private POIXMLReader pxr;
	private SynergyXMLReader sxr;
	private MapView browser;
	private StackPane content;
	private Group slideShowPane;
	private KeyPane key;
	private Pane container;
	private UniMap unimap;
	private String slideShow;
	private Tab route, info;
	private GridPane grid;
	private String panoLoc;

	/**
	 * Main method, not much to say here.
	 * 
	 * @param args
	 * 
	 */
	public static void main(String[] args) {

		// start up the GUI
		launch();

	}

	@Override
	public void start(final Stage stage) throws Exception {

		container = new Pane();
		container.setMaxSize(800, 600);

		tp = new TabPane();
		tp.setMaxSize(270, 600);

		content = new StackPane();

		// set tab names and properties
		route = new Tab("Route");
		route.setClosable(false);
		info = new Tab("Info");
		info.setClosable(false);

		// Instantiate Key
		key = new KeyPane();
		key.setVisible(false);
		key.setLayoutX(800); // Set the key to the top right of content pane.

		slideShowPane = new Group();

		container.getChildren().addAll(content, key, slideShowPane,
				Loading.getPane(), Alert.getPane());
		// hb.getChildren().addAll(container, tp);

		grid = new GridPane();
		grid.setPrefSize(1070, 600);
		GridPane.setRowIndex(container, 0);
		GridPane.setColumnIndex(container, 0);
		GridPane.setRowIndex(tp, 0);
		GridPane.setColumnIndex(tp, 1);

		grid.getChildren().addAll(container, tp);

		unimap = this;

		// scene, stage, show
		stage.setScene(new Scene(grid));
		stage.setResizable(false);
		stage.setTitle("Synergy UniMap");
		stage.show();

		Task<Void> parsingTask = new Task<Void>() {
			@Override
			public Void call() {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						Loading.showProgress();
					}
				});

				pxr = new POIXMLReader(
						"http://benshouse.crabdance.com/xml/HeadPOIXML.xml");
				pxr.readXMLFile("http://benshouse.crabdance.com/xml/HeadPOIXML.xml");

				return null;
			}

			@Override
			protected void succeeded() {
				super.succeeded();
				browser = new MapView(pxr.getHeadPOIList(), unimap);
				route.setContent(new RouteFinderPane(pxr.getHeadPOIList(),
						unimap, browser));
				info.setContent(new POIInfoPane(pxr.getHeadPOIList(), browser));
				content.getChildren().add(browser.returnBrowser());
				tp.getTabs().addAll(route, info);
				Loading.hideProgress();

				key.setVisible(true);
			}
		};
		new Thread(parsingTask).start();
	}

	/**
	 * Method is called to find and display a specific poi slide show, this is
	 * called from MyBrowser to interact with the javascript
	 */
	public void displayPoiSlideshow(String ss) {

		this.slideShow = ss;

		Task<Void> task = new Task<Void>() {
			@Override
			public Void call() {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						Loading.showProgress();
					}
				});

				// creates slideshow specific to icon pressed, i.e. icon pressed
				// is variable ss
				try {
					sxr = new SynergyXMLReader();
					sxr.progressProperty().addListener(new ChangeListener<Number>() {
						@Override
						public void changed(
								ObservableValue<? extends Number> arg0,
								Number ov, Number nv) {
							Loading.updateProgress(nv.doubleValue());
						}
					});
					
					sxr.fileNameProperty().addListener(new ChangeListener<String>(){
						@Override
						public void changed(
								ObservableValue<? extends String> arg0,
								String of, String nf) {
							Loading.showMessage(nf);
						}
					});
					
					sxr.parseFile("http://benshouse.crabdance.com/xml/POI/"
							+ slideShow + ".xml");
				} catch (SAXParseException pe) {
					Alert.displayError(pe.getMessage()+" @line: "+pe.getLineNumber());
				}catch (SAXException e) {
					Alert.displayError(e.getMessage());
				} 

				return null;
			}

			@Override
			protected void succeeded() {
				super.succeeded();

				if(!Loading.getKillThread()){
					alss = new SlideShow(sxr.getPresentable());
					
					System.out.println(sxr.getPresentable().getPresentations().size());
					
					alss.setOrientation(Orientation.VERTICAL);
		
					// display and play :)
					slideShowPane.getChildren().add(alss);
					alss.play();

					Loading.hideProgress();
				}
				
			}

		};
		new Thread(task).start();

	}

	/**
	 * Method is called to display a panoramic slide show, this is called from
	 * MyBrowser to interact with the javascript
	 */
	public void displayPanoSlideshow(final String poiPano) {

		System.out.println(poiPano);

		// Searches for the required poi, to display that poi's panoramic image
		for (int i = 0; i < pxr.getHeadPOIList().size(); i++) {

			panoLoc = null;

			if (poiPano.equals(pxr.getHeadPOIList().get(i).getName())) {

				if (pxr.getHeadPOIList().get(i).getPanoPhoto() != null) {
					panoLoc = pxr.getHeadPOIList().get(i).getPanoPhoto();
				} else {
					System.out.println("No Panorama found");
				}
			}

			if (panoLoc != null) {
				pv = new PanoViewer(panoLoc, 800, 600);
				slideShowPane.getChildren().add(pv);
			}

		}

	}

	/**
	 * This method sets the stackpane to display a given slideshow
	 * 
	 * @param slideshow
	 *            the slideshow to be displayed
	 */
	public void runRoute(SlideShow slideshow) {
		alss = slideshow;
		slideShowPane.getChildren().add(alss);
		alss.play();
	}

	/**
	 * This adds a new browser to the stackpane...?
	 */
	public void resetBrowser() {
		content.getChildren().addAll(browser.returnBrowser());
	}
}
