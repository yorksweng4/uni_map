package com.synergy.unimap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import com.synergy.slideshow.slide.Slide;

/**
 * Class Dijkstra's - This class computes the shortest route between a set of nodes. The nodes have 
 * been assigned to places on a floorplan image. The class also creates lines(rectangles) along each node path on the routes 
 * but all are set to visible false, they are only visible if the node is a long the shortest path. 
 * Each room has been assigned an node, which is stored in the xml. Each floorplan and its lines(rectangles) 
 * are added to seperate slides. In UniMap.java, the correct floorplan will be displayed depending on the end node choice. 
 * 
 * @Author Josh 
 */
public class Dijkstras {        
	// 2 dimensional array to connect lines between nodes 
	private Rectangle[][] rectArray = new Rectangle[70][70];
	// Different slides for each route
	private Slide elecOfficeRoute, elec1Route, elec2Route, elec3Route, elec4Route, elecConRoute;
	private ArrayList <Vertex> vertices;
	private ArrayList<Slide> routeSlides;

	public Dijkstras(){
		
		String floorURL = "http://benshouse.crabdance.com/xml/POI/ExhibitionCentre/";

		routeSlides = new ArrayList<Slide>();
		//Insert image, This will come from the XML eventually 
		ImageView elecOffices = new ImageView(new Image(floorURL+"offices/elec-floor.jpg", 800, 580, false, false));
		ImageView elecCon = new ImageView(new Image(floorURL+"0/elec-concourse.jpg", 800, 600, false, false));
		ImageView elec1 = new ImageView(new Image(floorURL+"1/elec-1.jpg", 800, 600, false, false));
		ImageView elec2 = new ImageView(new Image(floorURL+"2/elec-2.jpg", 800, 600, false, false));
		ImageView elec3 = new ImageView(new Image(floorURL+"3/elec-3.jpg", 800, 600, false, false));
		ImageView elec4 = new ImageView(new Image(floorURL+"4/elec-4.jpg", 800, 600, false, false));
		//TODO Add floor plan images here, physics and linguistics


		// Create the lines between each node and place into an 2 dimensional array 
		// Electronic offices lines 
		Rectangle rectr1 = createRectRedX(0,80);
		rectArray[1][2] = rectArray[2][1] = rectr1;
		Rectangle rectr2 = createRectRedX(110,80);
		rectArray[2][3] = rectArray[3][2] = rectr2;
		Rectangle rectr3 = createRectRedX(220,80);
		rectArray[3][4] = rectArray[4][3] = rectr3;
		Rectangle rectr4 = createRectRedX(330,80);
		rectArray[4][5] = rectArray[5][4] = rectr4;
		Rectangle rectr5 = createRectRedX(440,80);
		rectArray[5][6] = rectArray[6][5] = rectr5;
		Rectangle rectr6 = createRectRedXL(550,80);
		rectArray[6][7] = rectArray[7][6] = rectr6;
		// Y Lines 
		Rectangle rectr7 = createRectRedY(108,80);
		rectArray[2][8] = rectArray[8][2] = rectr7;
		Rectangle rectr8 = createRectRedY(330,80);
		rectArray[4][9] = rectArray[9][4] = rectr8;
		Rectangle rectr9 = createRectRedY(550,80);
		rectArray[6][10] = rectArray[10][6] = rectr9;
		Rectangle rectr10 = createRectRedY(108,185);
		rectArray[8][11] = rectArray[11][8] = rectr10;
		Rectangle rectr11 = createRectRedY(330,185);
		rectArray[9][13] = rectArray[13][9] = rectr11;
		Rectangle rectr12 = createRectRedY(550,185);
		rectArray[10][15] = rectArray[15][10] = rectr12;
		Rectangle rectr17 = createRectRedY(108,300);
		rectArray[11][16] = rectArray[16][11] = rectr17;
		Rectangle rectr18 = createRectRedY(330,300);
		rectArray[13][17] = rectArray[17][13] = rectr18;
		Rectangle rectr19 = createRectRedYS(108,415);
		rectArray[16][18] = rectArray[18][16] = rectr19;
		Rectangle rectr20 = createRectRedYS(330,415);
		rectArray[17][21] = rectArray[21][17] = rectr20;
		// Middle X lines
		Rectangle rectr13 = createRectRedX(110,290);
		rectArray[11][12] = rectArray[12][11] = rectr13;
		Rectangle rectr14 = createRectRedX(220,290);
		rectArray[12][13] = rectArray[13][12] = rectr14;
		Rectangle rectr15 = createRectRedX(330,290);
		rectArray[13][14] = rectArray[14][13] = rectr15;
		Rectangle rectr16 = createRectRedX(440,290);
		rectArray[14][15] = rectArray[15][14] = rectr16;

		// bottom x lines 
		Rectangle rectr21 = createRectRedX(108,500);
		rectArray[18][19] = rectArray[19][18] = rectr21;
		Rectangle rectr22 = createRectRedX(185,500);
		rectArray[19][20] = rectArray[20][19] = rectr22;
		Rectangle rectr23 = createRectRedX(230,500);
		rectArray[20][21] = rectArray[21][20] = rectr23;
		Rectangle rectr24 = createRectRedX(315,500);
		rectArray[21][22] = rectArray[22][21] = rectr24;

		// bottom y lines in z block 
		Rectangle rectr25 = createRectRedYSS(260,500);
		rectArray[20][23] = rectArray[23][20] = rectr25;
		Rectangle rectr26 = createRectRedYSS(340,500);
		rectArray[21][24] = rectArray[24][21] = rectr26;
		// Electronics Floor 1 lines, nodes 25-30
		Rectangle rectr27 = createRectRedX(540,155);
		rectArray[25][26] = rectArray[26][25] = rectr27;
		Rectangle rectr28 = createRectRedY(540,160);
		rectArray[26][27] = rectArray[27][26] = rectr28;
		Rectangle rectr29 = createRectRedY(540,275);
		rectArray[27][28] = rectArray[28][27] = rectr29;
		Rectangle rectr30 = createRectRedX(205,390);
		rectArray[29][30] = rectArray[30][29] = rectr30;
		Rectangle rectr31 = createRectRedX(435,390);
		rectArray[28][29] = rectArray[29][28] = rectr31;
		Rectangle rectr32 = createRectRedX(320,390);
		rectArray[30][31] = rectArray[31][30] = rectr32;

		// Electronics Floor 2 lines, nodes 32 - 38
		Rectangle rectr33 = createRectRedX(540,160);
		rectArray[32][33] = rectArray[33][32] = rectr33;
		Rectangle rectr34 = createRectRedXL(390,160);
		rectArray[33][34] = rectArray[34][33] = rectr34;
		Rectangle rectr35 = createRectRedXL(240,160);
		rectArray[34][35] = rectArray[35][34] = rectr35;
		Rectangle rectr36 = createRectRedXL(125,160);
		rectArray[35][36] = rectArray[36][35] = rectr36;
		Rectangle rectr37 = createRectRedY(600,160);
		rectArray[32][37] = rectArray[37][32] = rectr37;
		Rectangle rectr38 = createRectRedYS(600,265);
		rectArray[37][38] = rectArray[38][37] = rectr38;

		// Electronics 3 floor 39-
		Rectangle rectr39 = createRectRedY(600,215);
		rectArray[39][40] = rectArray[40][39] = rectr39;

		//Electronics 4th floor 
		Rectangle rectr40 = createRectRedY(540,160);
		rectArray[41][42] = rectArray[42][41] = rectr40;

		// Electronics Concourse
		Rectangle rectr41 = createRectRedY(550,5);
		rectArray[43][44] = rectArray[44][43] = rectr41;
		Rectangle rectr42 = createRectRedY(550,90);
		rectArray[44][45] = rectArray[45][44] = rectr42;
		Rectangle rectr43 = createRectRedX(425,195);
		rectArray[45][46] = rectArray[46][45] = rectr43;
		Rectangle rectr44 = createRectRedY(425,205);
		rectArray[46][47] = rectArray[47][46] = rectr44;
		Rectangle rectr45 = createRectRedX(310,310);
		rectArray[47][48] = rectArray[48][47] = rectr45; 
		Rectangle rectr46 = createRectRedX(425,315);
		rectArray[47][49] = rectArray[49][47] = rectr46;
		Rectangle rectr47 = createRectRedY(425,320);
		rectArray[47][50] = rectArray[50][47] = rectr47;

		// Create an arrayList of Vertex's 
		vertices = new ArrayList<Vertex>();
		//initialize graph, should be done in a loop!!
		Vertex v1 = new Vertex("1");
		vertices.add(v1);
		Vertex v2 = new Vertex("2");
		vertices.add(v2);
		Vertex v3 = new Vertex("3");
		vertices.add(v3);
		Vertex v4 = new Vertex("4");
		vertices.add(v4);
		Vertex v5 = new Vertex("5");
		vertices.add(v5);
		Vertex v6 = new Vertex("6");
		vertices.add(v6);
		Vertex v7 = new Vertex("7");
		vertices.add(v7);
		Vertex v8 = new Vertex("8");
		vertices.add(v8);
		Vertex v9 = new Vertex("9");
		vertices.add(v9);
		Vertex v10 = new Vertex("10");
		vertices.add(v10);
		Vertex v11 = new Vertex("11");
		vertices.add(v11);
		Vertex v12 = new Vertex("12");
		vertices.add(v12);
		Vertex v13 = new Vertex("13");
		vertices.add(v13);
		Vertex v14 = new Vertex("14");
		vertices.add(v14);
		Vertex v15 = new Vertex("15");
		vertices.add(v15);
		Vertex v16 = new Vertex("16");
		vertices.add(v16);
		Vertex v17 = new Vertex("17");
		vertices.add(v17);
		Vertex v18 = new Vertex("18");
		vertices.add(v18);
		Vertex v19 = new Vertex("19");
		vertices.add(v19);
		Vertex v20 = new Vertex("20");
		vertices.add(v20);
		Vertex v21 = new Vertex("21");
		vertices.add(v21);
		Vertex v22 = new Vertex("22");
		vertices.add(v22);
		Vertex v23 = new Vertex("23");
		vertices.add(v23);
		Vertex v24 = new Vertex("24");
		vertices.add(v24);
		// Vertices for floor 1
		Vertex v25 = new Vertex("25");
		vertices.add(v25);
		Vertex v26 = new Vertex("26");
		vertices.add(v26);
		Vertex v27 = new Vertex("27");
		vertices.add(v27);
		Vertex v28 = new Vertex("28");
		vertices.add(v28);
		Vertex v29 = new Vertex("29");
		vertices.add(v29);
		Vertex v30 = new Vertex("30");
		vertices.add(v30);
		Vertex v31 = new Vertex("31");
		vertices.add(v31);
		// Vertices for floor 2 
		Vertex v32 = new Vertex("32");
		vertices.add(v32);
		Vertex v33 = new Vertex("33");
		vertices.add(v33);
		Vertex v34 = new Vertex("34");
		vertices.add(v34);
		Vertex v35 = new Vertex("35");
		vertices.add(v35);
		Vertex v36 = new Vertex("36");
		vertices.add(v36);
		Vertex v37 = new Vertex("37");
		vertices.add(v37);
		Vertex v38 = new Vertex("38");
		vertices.add(v38);
		// 3rd floor 
		Vertex v39 = new Vertex("39");
		vertices.add(v39);
		Vertex v40 = new Vertex("40");
		vertices.add(v40);
		// 4t floor 
		Vertex v41 = new Vertex("41");
		vertices.add(v41);
		Vertex v42 = new Vertex("42");
		vertices.add(v42);
		// Electronics Concourse
		Vertex v43 = new Vertex("43");
		vertices.add(v43);
		Vertex v44 = new Vertex("44");
		vertices.add(v44);
		Vertex v45 = new Vertex("45");
		vertices.add(v45);
		Vertex v46 = new Vertex("46");
		vertices.add(v46);
		Vertex v47 = new Vertex("47");
		vertices.add(v47);
		Vertex v48 = new Vertex("48");
		vertices.add(v48);
		Vertex v49 = new Vertex("49");
		vertices.add(v49);
		Vertex v50 = new Vertex("50");
		vertices.add(v50);

		// Create the edges for each node and add to vertices, some nodes have 1, 2, 3 and 4 edges
		v1.setAdjacencies(new Edge[]{ new Edge(v2,  10, 1 ),
				new Edge(v1,0,1)});
		v2.setAdjacencies(new Edge[]{ new Edge(v1,  10, 2 ),
				new Edge(v3,  10, 2 ),
				new Edge(v8, 10, 2)
		});
		v3.setAdjacencies(new Edge[]{ new Edge(v2, 10, 3 ),
				new Edge(v4, 10, 3)});
		v4.setAdjacencies(new Edge[]{ new Edge(v3, 10, 4), 
				new Edge(v5, 10, 4),
				new Edge(v9, 10, 4)});

		v5.setAdjacencies(new Edge[]{ new Edge(v4, 10, 5), 
				new Edge(v6, 10, 5)});

		v6.setAdjacencies(new Edge[]{new Edge(v5, 10,6),
				new Edge(v7, 10, 6),
				new Edge(v10, 10, 6)});
		v7.setAdjacencies(new Edge[]{new Edge(v6, 10, 7),
				new Edge(v7,0,7)});
		v8.setAdjacencies(new Edge[]{new Edge(v2, 10, 8),
				new Edge(v11,10,8)});
		v9.setAdjacencies(new Edge[]{new Edge(v4, 10, 9),
				new Edge(v13,10,9)});
		v10.setAdjacencies(new Edge[]{new Edge(v6, 10, 10),
				new Edge(v15,10,10)});
		v11.setAdjacencies(new Edge[]{new Edge(v8, 10, 11),
				new Edge(v12,10,11),
				new Edge(v16,10,11)});
		v12.setAdjacencies(new Edge[]{new Edge(v11, 10, 12),
				new Edge(v13,10,12)});
		v13.setAdjacencies(new Edge[]{new Edge(v9, 10, 13),
				new Edge(v12,10,13),
				new Edge(v14,10,13),
				new Edge(v17,10,13)});
		v14.setAdjacencies(new Edge[]{new Edge(v13, 10, 14),
				new Edge(v15,10,14)});
		v15.setAdjacencies(new Edge[]{new Edge(v10, 10, 15),
				new Edge(v14,10,15)});
		v16.setAdjacencies(new Edge[]{new Edge(v11, 10, 16),
				new Edge(v18,8,16)});
		v17.setAdjacencies(new Edge[]{new Edge(v13, 10, 17),
				new Edge(v21,8,17)});
		v18.setAdjacencies(new Edge[]{new Edge(v16, 10, 18),
				new Edge(v19,5, 18)});
		v19.setAdjacencies(new Edge[]{new Edge(v18, 5, 19),
				new Edge(v20,5,19)});
		v20.setAdjacencies(new Edge[]{new Edge(v19, 5, 20),
				new Edge(v21,5,20),
				new Edge(v23,4,20)});
		v21.setAdjacencies(new Edge[]{new Edge(v20, 5, 21),
				new Edge(v22,5,21),
				new Edge(v17,5,21),
				new Edge(v24,4,21)});
		v22.setAdjacencies(new Edge[]{new Edge(v21, 5, 22),
				new Edge(v22,0,22)});
		v23.setAdjacencies(new Edge[]{new Edge(v20, 4, 23),
				new Edge(v23,0,23)});
		v24.setAdjacencies(new Edge[]{new Edge(v21, 4, 24),
				new Edge(v24,0,24)});
		// Floor 1
		v25.setAdjacencies(new Edge[]{new Edge(v26, 3, 25),
				new Edge(v25,0,25)});
		v26.setAdjacencies(new Edge[]{new Edge(v25, 3, 26),
				new Edge(v27,3,26)});
		v27.setAdjacencies(new Edge[]{new Edge(v26, 3, 27),
				new Edge(v28,3,27)});
		v28.setAdjacencies(new Edge[]{new Edge(v27, 3, 28),
				new Edge(v29,3,28)});
		v29.setAdjacencies(new Edge[]{new Edge(v28, 3, 29),
				new Edge(v30,3,29)});
		v30.setAdjacencies(new Edge[]{new Edge(v29, 3, 30),
				new Edge(v31,3,30)});
		v31.setAdjacencies(new Edge[]{new Edge(v30, 3, 31),
				new Edge(v31,0,31)});
		//TODO add and assign new vertices here
		// Nodes for fllor 2
		v32.setAdjacencies(new Edge[]{new Edge(v33, 3, 32),
				new Edge(v37,3,32)});
		v33.setAdjacencies(new Edge[]{new Edge(v32, 3, 33),
				new Edge(v34,3,33)});
		v34.setAdjacencies(new Edge[]{new Edge(v35, 3, 34),
				new Edge(v34,3,34)});
		v35.setAdjacencies(new Edge[]{new Edge(v34, 3, 35),
				new Edge(v36,3,35)});
		v36.setAdjacencies(new Edge[]{new Edge(v35, 3, 36),
				new Edge(v36,0,36)});
		v37.setAdjacencies(new Edge[]{new Edge(v32, 3, 37),
				new Edge(v38,3,37)});
		v38.setAdjacencies(new Edge[]{new Edge(v37, 3, 38),
				new Edge(v38,0,38)});
		v39.setAdjacencies(new Edge[]{new Edge(v40, 3, 39),
				new Edge(v39,0,39)});
		v40.setAdjacencies(new Edge[]{new Edge(v39, 3, 40),
				new Edge(v40,0,40)});
		v41.setAdjacencies(new Edge[]{new Edge(v42, 3, 41),
				new Edge(v41,0,41)});
		v42.setAdjacencies(new Edge[]{new Edge(v41, 3, 42),
				new Edge(v42,0,42)});
		// Concourse
		v43.setAdjacencies(new Edge[]{new Edge(v44, 3, 43),
				new Edge(v43,0,43)});
		v44.setAdjacencies(new Edge[]{new Edge(v45, 3, 44),
				new Edge(v43,3,44)});
		v45.setAdjacencies(new Edge[]{new Edge(v46, 3, 45),
				new Edge(v44,3,45)});
		v46.setAdjacencies(new Edge[]{new Edge(v47, 3, 46),
				new Edge(v45,3,46)});
		v47.setAdjacencies(new Edge[]{new Edge(v46, 3, 47),
				new Edge(v48,3,47),
				new Edge(v49,3,47),
				new Edge(v50,3,47)});
		v48.setAdjacencies(new Edge[]{new Edge(v47, 3, 48),
				new Edge(v48,0,48)});
		v49.setAdjacencies(new Edge[]{new Edge(v47, 3, 49),
				new Edge(v49,0,49)});
		v50.setAdjacencies(new Edge[]{new Edge(v47, 3, 50),
				new Edge(v50,0,50)});

		//Add content to slides for electronics offices 
		elecOfficeRoute = new Slide("Electronics Offices", 0, 800, 600);
		elecOfficeRoute.setStyle("-fx-background-color: #"+"FFFFFF");
		Pane elecOfficePane = new Pane();
		// Add all the lines onto the image in the slide
		elecOfficePane.getChildren().addAll(elecOffices,rectr1,rectr2,rectr3,rectr4,rectr5,rectr6,rectr7,rectr8,
				rectr9,rectr10,rectr11,rectr12,rectr13,rectr14,rectr15,rectr16,rectr17,rectr18,rectr19,rectr20,
				rectr21,rectr22,rectr23,rectr24,rectr25,rectr26);
		elecOfficePane.setLayoutX(0);
		elecOfficeRoute.getChildren().add(elecOfficePane);
		routeSlides.add(elecOfficeRoute);

		// Add content to slides for electronic floors 
		elec1Route = new Slide("Electronics First Floor", 0, 800, 600);
		elec1Route.setStyle("-fx-background-color: #"+"FFFFFF");
		Pane elec1Pane = new Pane();
		// add all the lines onto the image floorplan
		elec1Pane.getChildren().addAll(elec1,rectr27,rectr28,rectr29,rectr30,rectr31,rectr32);
		elec1Pane.setLayoutX(0);
		elec1Route.getChildren().add(elec1Pane);
		routeSlides.add(elec1Route);

		// Add content to slides for electronic floors 
		elec2Route = new Slide("Electronics Second Floor", 0, 800, 600);
		elec2Route.setStyle("-fx-background-color: #"+"FFFFFF");
		Pane elec2Pane = new Pane();
		// add all the lines onto the image floorplan
		elec2Pane.getChildren().addAll(elec2,rectr33,rectr34,rectr35,rectr36,rectr37,rectr38);
		elec2Pane.setLayoutX(0);
		elec2Route.getChildren().add(elec2Pane);
		routeSlides.add(elec2Route);

		// Add content to slides for electronic floors 
		elec3Route = new Slide("Electronics Third Floor", 0, 800, 600);
		elec3Route.setStyle("-fx-background-color: #"+"FFFFFF");
		Pane elec3Pane = new Pane();
		// add all the lines onto the image floorplan
		elec3Pane.getChildren().addAll(elec3,rectr39);
		elec3Pane.setLayoutX(0);
		elec3Route.getChildren().add(elec3Pane);
		routeSlides.add(elec3Route);
		
		// Add content to slides for electronic floors 
		elec4Route = new Slide("Electronics Third Floor", 0, 800, 600);
		elec4Route.setStyle("-fx-background-color: #"+"FFFFFF");
		Pane elec4Pane = new Pane();
		// add all the lines onto the image floorplan
		elec4Pane.getChildren().addAll(elec4,rectr40);
		elec4Pane.setLayoutX(0);
		elec4Route.getChildren().add(elec4Pane);
		routeSlides.add(elec4Route);

		// Add content to slides for electronic floors 
		elecConRoute = new Slide("Electronics Concourse", 0, 800, 600);
		elecConRoute.setStyle("-fx-background-color: #"+"FFFFFF");
		Pane elecConPane = new Pane();
		// add all the lines onto the image floorplan
		elecConPane.getChildren().addAll(elecCon,rectr41,rectr42,rectr43,rectr44,rectr45,rectr46,rectr47);
		elecConPane.setLayoutX(0);
		elecConRoute.getChildren().add(elecConPane);
		routeSlides.add(elecConRoute);
	}

	/**
	 *  Method to return the routeSlides 
	 * @return routeSlides
	 */
	public ArrayList<Slide> getRouteSlides(){
		return routeSlides;
	}

	/**
	 * Gets the vertices 
	 * @param i
	 * @return vertices
	 */
	public Vertex getVertex(int i){

		return vertices.get(i-1);
	}

	/**
	 *  Method to compute the route for the electronics Offices 
	 * @param to
	 * @param from
	 * @return elecOfficeRoute
	 */
	public Slide createOfficeRoute(Vertex to, Vertex from){

		computePaths(from);

		// print out shortest path and distance between v1 and v2
		System.out.println("[Offices] Shortest Distance From " + from + " to " + to + ": " + to.getMinDistance() + " metres");
		List<Vertex> path = getShortestPathTo(to);
		System.out.println("Path: " + path); 

		for(int q=0; q<path.size()-1; q++){

			Vertex row = path.get(q);
			Edge[] a = row.getAdjacencies();
			Edge[] w = path.get(q+1).getAdjacencies();

			int b = w[0].getNode();
			int d = a[1].getNode();

			System.out.println("Node " + d + " to Node "+ b);
			// The nodes in the path are then set visible, 
			// so the red rectangle lines will be visible 
			rectArray[d][b].setVisible(true);

		}
		return elecOfficeRoute; 
	}

	/**
	 *  Method to compute the route for the electronics Concourse 
	 * @param to
	 * @param from
	 * @return elecConRoute
	 */
	public Slide createElecConRoute(Vertex to, Vertex from){

		computePaths(from);

		// print out shortest path and distance between v1 and v2
		System.out.println("[Concourse] Shortest Distance From " + from + " to " + to + ": " + to.getMinDistance() + " metres");
		List<Vertex> path = getShortestPathTo(to);
		System.out.println("Path: " + path); 

		for(int q=0; q<path.size()-1; q++){

			Vertex row = path.get(q);
			Edge[] a = row.getAdjacencies();
			Edge[] w = path.get(q+1).getAdjacencies();

			int b = w[0].getNode();
			int d = a[1].getNode();

			System.out.println("Node " + d + " to Node "+ b);
			// The nodes in the path are then set visible, 
			// so the red rectangle lines will be visible 
			rectArray[d][b].setVisible(true);

		}
		return elecConRoute;    
	}

	/**
	 *  Method to compute the route for the electronics 1st floor 
	 * @param to
	 * @param from
	 * @return elec1Route
	 */
	public Slide createElec1Route(Vertex to, Vertex from){

		computePaths(from);

		// print out shortest path and distance between v1 and v2
		System.out.println("[Floor 1] Shortest Distance From " + from + " to " + to + ": " + to.getMinDistance() + " metres");
		List<Vertex> path = getShortestPathTo(to);
		System.out.println("Path: " + path); 

		for(int q=0; q<path.size()-1; q++){

			Vertex row = path.get(q);
			Edge[] a = row.getAdjacencies();
			Edge[] w = path.get(q+1).getAdjacencies();

			int b = w[0].getNode();
			int d = a[1].getNode();

			System.out.println("Node " + d + " to Node "+ b);
			// The nodes in the path are then set visible, 
			// so the red rectangle lines will be visible 
			rectArray[d][b].setVisible(true);

		}
		return elec1Route;
	}

	/**
	 *  Method to compute the route for the elctronics 2nd floor 
	 * @param to
	 * @param from
	 * @return elec2Route
	 */
	public Slide createElec2Route(Vertex to, Vertex from){

		computePaths(from);

		// print out shortest path and distance between v1 and v2
		System.out.println("[Floor 2] Shortest Distance From " + from + " to " + to + ": " + to.getMinDistance() + " metres");
		List<Vertex> path = getShortestPathTo(to);
		System.out.println("Path: " + path); 

		for(int q=0; q<path.size()-1; q++){

			Vertex row = path.get(q);
			Edge[] a = row.getAdjacencies();
			Edge[] w = path.get(q+1).getAdjacencies();

			int b = w[0].getNode();
			int d = a[1].getNode();

			System.out.println("Node " + d + " to Node "+ b);
			// The nodes in the path are then set visible, 
			// so the red rectangle lines will be visible 
			rectArray[d][b].setVisible(true);

		}
		return elec2Route;      
	}

	/**
	 *  Method to compute the route for the elctronics 3nd floor
	 * @param to
	 * @param from
	 * @return elec3Route
	 */
	public Slide createElec3Route(Vertex to, Vertex from){

		computePaths(from);

		// print out shortest path and distance between v1 and v2
		System.out.println("[Floor 3] Shortest Distance From " + from + " to " + to + ": " + to.getMinDistance() + " metres");
		List<Vertex> path = getShortestPathTo(to);
		System.out.println("Path: " + path); 

		for(int q=0; q<path.size()-1; q++){

			Vertex row = path.get(q);
			Edge[] a = row.getAdjacencies();
			Edge[] w = path.get(q+1).getAdjacencies();

			int b = w[0].getNode();
			int d = a[1].getNode();

			System.out.println("Node " + d + " to Node "+ b);
			// The nodes in the path are then set visible, 
			// so the red rectangle lines will be visible 
			rectArray[d][b].setVisible(true);

		}
		return elec3Route;      
	}

	/**
	 *  Method to compute the route for the elctronics 4nd floor
	 * @param to
	 * @param from
	 * @return elec4Route
	 */
	public Slide createElec4Route(Vertex to, Vertex from){

		computePaths(from);

		// print out shortest path and distance between v1 and v2
		System.out.println("[Floor 4] Shortest Distance From " + from + " to " + to + ": " + to.getMinDistance() + " metres");
		List<Vertex> path = getShortestPathTo(to);
		System.out.println("Path: " + path); 

		for(int q=0; q<path.size()-1; q++){

			Vertex row = path.get(q);
			Edge[] a = row.getAdjacencies();
			Edge[] w = path.get(q+1).getAdjacencies();

			int b = w[0].getNode();
			int d = a[1].getNode();

			System.out.println("Node " + d + " to Node "+ b);
			// The nodes in the path are then set visible, 
			// so the red rectangle lines will be visible 
			rectArray[d][b].setVisible(true);

		}
		return elec4Route;      
	}

	/**
	 *  Method to create a horizontal Red rectangle(line), visible set to false 
	 * @param x
	 * @param y
	 * @return rect
	 */
	public Rectangle createRectRedX (int x, int y){

		Rectangle rect = new Rectangle(x, y, 115, 10);
		rect.setFill(Color.RED);
		rect.setVisible(false);

		return rect;

	}

	/**
	 *  Method to create a smaller horizontal Red rectangle(line), visible set to false
	 * @param x
	 * @param y
	 * @return rect
	 */
	public Rectangle createRectRedXS (int x, int y){

		Rectangle rect = new Rectangle(x, y, 60, 10);
		rect.setFill(Color.RED);
		rect.setVisible(false);

		return rect;

	}
	/**
	 *  Method to create a larger horizontal Red rectangle(line), visible set to false
	 * @param x
	 * @param y
	 * @return rect
	 */
	public Rectangle createRectRedXL (int x, int y){

		Rectangle rect = new Rectangle(x, y, 150, 10);
		rect.setFill(Color.RED);
		rect.setVisible(false);

		return rect;

	}
	/**
	 *  Method to create a Vertical Red rectangle(line), visible set to false
	 * @param x
	 * @param y
	 * @return rect
	 */
	public Rectangle createRectRedY (int x, int y){

		Rectangle rect = new Rectangle(x, y, 10, 115);
		rect.setFill(Color.RED);
		rect.setVisible(false);
		return rect;

	}       
	/**
	 *  Method to create a smaller Vertical Red rectangle(line), visible set to false
	 * @param x
	 * @param y
	 * @return rect
	 */
	public Rectangle createRectRedYS (int x, int y){

		Rectangle rect = new Rectangle(x, y, 10, 90);
		rect.setFill(Color.RED);
		rect.setVisible(false);
		return rect;

	}
	/**
	 *  Method to create a smaller Vertical Red rectangle(line), visible set to false
	 * @param x
	 * @param y
	 * @return rect
	 */
	public Rectangle createRectRedYSS (int x, int y){

		Rectangle rect = new Rectangle(x, y, 10, 50);
		rect.setFill(Color.RED);
		rect.setVisible(false);
		return rect;

	}

	/**
	 *  Creates a list of Vertex's, which holds the path for the shortest route
	 * @param target
	 * @return path
	 */
	public List<Vertex> getShortestPathTo(Vertex target)
	{
		List<Vertex> path = new ArrayList<Vertex>();
		for (Vertex vertex = target; vertex != null; vertex = vertex.getPrevious())
			// adds the vertex's to the path 
			path.add(vertex);

		Collections.reverse(path);
		return path;
	}
	/**
	 * Method to compute the shortest route 
	 * @param source
	 */
	public void computePaths(Vertex source)
	{
		source.setMinDistance(0.);
		//visit each vertex u, always visiting vertex with smallest minDistance first
		PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
		vertexQueue.add(source);

		while (!vertexQueue.isEmpty()) {
			Vertex u = vertexQueue.poll();
			// Visit each edge exiting u
			for (Edge e : u.getAdjacencies())
			{
				Vertex v = e.getTarget();
				double weight = e.getWeight();
				// relax the edge (u,v)
				double distanceThroughU = u.getMinDistance() + weight;
				if (distanceThroughU < v.getMinDistance()) {
					// remove v from queue
					vertexQueue.remove(v);

					v.setMinDistance(distanceThroughU) ;
					v.setPrevious(u);

					// re-add v to queue
					vertexQueue.add(v);
				}
			}
		}
	}
}