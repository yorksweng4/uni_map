package com.synergy.unimap;

/**
 * A class for Edge which holds its attributes
 * @Author Josh 
 */
class Edge
{
    private final Vertex target;
    private final double weight;
    private final int node;
    
    /**
     * 
     * @param argTarget
     * @param argWeight
     * @param myNode
     */
    public Edge(Vertex argTarget, double argWeight, int myNode){
        
        target = argTarget;
        weight = argWeight;
        node = myNode;

    }
    /**
     * 
     * @return node
     */
    public int theNode(){
        return getNode(); }
	/**
	 * @return the node
	 */
	public int getNode() {
		return node;
	}
	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}
	/**
	 * @return the target
	 */
	public Vertex getTarget() {
		return target;
	}

}