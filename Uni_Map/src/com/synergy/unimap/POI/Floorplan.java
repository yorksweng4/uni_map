package com.synergy.unimap.POI;

/**
 * holds floorplan image details
 * 
 * @author Rob, Kat
 *
 */

public class Floorplan {

	private String ID;
	private String filename;

	public Floorplan(String ID, String filename) {
		this.ID = ID;
		this.filename = filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return this.filename;
	}

	public String getID() {
		return this.ID;
	}

}
