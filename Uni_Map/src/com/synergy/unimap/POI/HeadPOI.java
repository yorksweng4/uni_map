package com.synergy.unimap.POI;

import java.util.List;

import org.xml.sax.Attributes;

/**
 * This class holds a list of POIs, as a head POI should do.
 * 
 * @author Kat, Ben
 *
 */
public class HeadPOI {
	
	private String name;
	private String iconFilename;
	private String tag;
	private Double lat, lng;

	private String panoLoc;
	private String photo;
	private List<PointOfInterest> listOfPOIs;
	
	/**
	 * @param attributes attributes from XML parser
	 */
	public HeadPOI(Attributes attributes) {

		//Defaults (location is lake centre)
		name = "DefaultPOI";
		iconFilename = "http://benshouse.crabdance.com/xml/icons/Department.png";
		tag = "Default tag line";
		lat = 53.946789;
		lng = -1.054092;
		photo = "http://benshouse.crabdance.com/xml/photos/imagenotfound.png";
		 
		
		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("Name")){
				if(!attributes.getValue(i).isEmpty()){
					name = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("lat")){
				if(!attributes.getValue(i).isEmpty()){
					lat = Double.parseDouble(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("lng")){
				if(!attributes.getValue(i).isEmpty()){
					lng = Double.parseDouble(attributes.getValue(i));
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("iconImage")){
				if(!attributes.getValue(i).isEmpty()){
					iconFilename = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("Tag")){
				if(!attributes.getValue(i).isEmpty()){
					tag = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("photo")){
				if(!attributes.getValue(i).isEmpty()){
					photo = attributes.getValue(i);
				}
			}
		}
		
	}
	
	/**
	 * Sets the URL of the panoramic photo for this POI
	 * 
	 * @param Photo URL as String
	 */
	public void setPanoLoc(String photo) {
		this.panoLoc = photo;
	}

	/**
	 *Gets the URL of the panoramic photo for this POI 
	 *
	 * @return URL of panoramic photo
	 */
	public String getPanoPhoto() {
		return this.panoLoc;
	}


	/**
	 * @param List PointOfInterest
	 */
	public void setPOIList(List<PointOfInterest> poi){
		this.listOfPOIs = poi;
	}
	
	/**
	 * 
	 * @param PointOfInterest
	 */
	public void addPoi(PointOfInterest poi){
		this.listOfPOIs.add(poi);
	}
	
	/**
	 * @return List PointOfInterest
	 */
	public List<PointOfInterest> getPOIList(){
		return listOfPOIs;
	}
	
	/**
	 *Gets latitude of POI
	 *
	 * @return lat
	 */
	public Double getLat(){
		return lat;
	}
	
	/**
	 *Gets longitude of POI
	 *
	 * @return longditude
	 */
	public Double getLng(){
		return lng;
	}
	
	/**
	 *Gets icon URL for POI
	 *
	 * @return Icon URL
	 */
	public String getIconFilename(){
		return iconFilename;
	}

	/**
	 *Gets POI name
	 *
	 * @return POI name
	 */
	public String getName() {
		return name;
	}

	/**
	 *Gets tag line for POI
	 *
	 * @return tage line
	 */
	public String getTag() {
		return tag;
	}
	
	/**
	 *Gets POI photo URL
	 *
	 * @return photo URL
	 */
	public String getPhoto(){
		return this.photo;
	}
	
	/**
	 *sets POI photo URL
	 *
	 * @param set photo URL
	 */
	public void setPhoto(String url){
		this.photo = url;
	}

}
