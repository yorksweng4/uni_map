/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.unimap.POI;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import com.synergy.slideshow.SlideShow;
import com.synergy.slideshow.SlideShowEvent;
import com.synergy.slideshow.presentable.Presentation;
import com.synergy.slideshow.slide.Slide;
import com.synergy.unimap.Dijkstras;
import com.synergy.unimap.Loading;
import com.synergy.unimap.MapView;
import com.synergy.unimap.UniMap;
import com.synergy.unimap.Vertex;

/**
 * This class implements 2 textFields for search functionality and a command
 * button. For each search field there is a relevant list that has the data
 * structure info. When a key is pressed the list is updated, making searching
 * easier
 * 
 * @author Josh Ferguson, Mike Holmes, Ben Cowperthwaite
 * 
 * 
 */

public class RouteFinderPane extends Pane {

	// Fields for functionality of buttons, textFields and lists

	private static TextField startSearch;
	private static TextField finishSearch;
	private Button go;

	private static boolean routeSlideShowPlayed = false;

	private List<HeadPOI> pois;
	private UniMap refUni;

	// slideshow
	private Slide route, firstImage, lastImage;
	private SlideShow alss;
	private ArrayList<Slide> slides;
	private Presentation pres;
	private Pane advertPane = new Pane();

	// Observable list for updating the list when a key is pressed
	private ObservableList<String> entries = FXCollections
			.observableArrayList();
	// start and finish list creations
	private ListView<String> startList = new ListView<String>();
	private ListView<String> finishList = new ListView<String>();

	private MapView ref;

	private int i, j, k, l, m, n, o, p;

	private Double startLng = 0.0;
	private Double startLat = 0.0;
	private Double endLng = 0.0;
	private Double endLat = 0.0;

	private int finalNode = 0;
	private int startNode = 0;

	private Dijkstras exroute;

	private ImageView POIImageView;
	private Image POIImage;

	private Slide floorRoutePane;

	public RouteFinderPane(List<HeadPOI> pois, UniMap parent, MapView ref) {

		super();

		this.refUni = parent;

		// get pois from parser
		this.pois = pois;
		this.ref = ref;

		// make the actual GUI part of it
		makeInterface();
	}

	/**
	 * textField box which has a listener implemented for key presses
	 * 
	 * @return the start search box which is implemented as a textField
	 */

	private TextField startSearch() {

		// Start search box
		Group root = new Group();
		startSearch = new TextField();
		startSearch.setPromptText("Start");
		startSearch.setMaxSize(105, 25);
		// Add a listener to observe the value on each key press
		startSearch.textProperty().addListener(new ChangeListener<Object>() {
			public void changed(ObservableValue<?> observable, Object oldVal,
					Object newVal) {
				handleSearchByKey2((String) oldVal, (String) newVal);
			}
		});

		startList.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {
				if (startList.getSelectionModel().getSelectedItem() != null) {
					startSearch.setText(startList.getSelectionModel()
							.getSelectedItem());
					System.out.println(startSearch.getText());
				}
			}

		});

		// Add the search box to the root
		root.getChildren().addAll(startSearch);

		return startSearch;
	}

	/**
	 * textField box which has a listener implemented for key presses
	 * 
	 * @return the start search box which is implemented as a textField
	 */
	private TextField finishSearch() {
		// Finish search box
		Group root = new Group();
		finishSearch = new TextField();
		finishSearch.setPromptText("Finish");
		finishSearch.setMaxSize(105, 25);

		finishSearch.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String oldVal, String newVal) {
				handleSearchByKey3(oldVal, newVal);

			}
		});

		finishList.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent e) {

				if (finishList.getSelectionModel().getSelectedItem() != null) {
					finishSearch.setText(finishList.getSelectionModel()
							.getSelectedItem());
					System.out.println(finishSearch.getText());
				}
			}

		});

		root.getChildren().addAll(finishSearch);

		return finishSearch;
	}

	/**
	 * Button to do the search command
	 * 
	 * @return the button
	 */
	private Button goButton() {
		// Command button
		go = new Button("GO");
		go.setMinSize(200, 50);
		go.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				Task<Void> task = new Task<Void>() {

					@Override
					protected void scheduled() {
						super.succeeded();
						Loading.showBusy();
						
						//If there's something already running, get rid
						if (alss != null){
							alss.pause();
						}

						// create slideshow
						slides = new ArrayList<Slide>();

						// create Slides
						route = new Slide("Cloud map route", 0, 800, 600);

						route.getChildren().add(ref.returnBrowser());
					}

					@Override
					public Void call() {

						createRouteSlideshow();

						return null;
					}

					@Override
					protected void succeeded() {
						super.succeeded();

						System.out.println("Continue?: "+Loading.getKillThread());
						
						if (!Loading.getKillThread()) {
							routeSlideShowPlayed = true;

							if (startLat != endLat && startLng != endLng) {
								System.out.println("12");
								System.out.println(ref.returnBrowser()
										.toString());
								System.out.println(startLat);
								System.out.println(startLng);
								System.out.println(endLat);
								System.out.println(endLng);
								ref.runRoute(startLat, startLng, endLat, endLng);
								System.out.println("13");

							}

							if (route != null) {
								slides.add(route);
							}
							if (firstImage != null) {
								slides.add(firstImage);
							}
							if (floorRoutePane != null) {
								slides.add(floorRoutePane);
							}
							if (lastImage != null) {
								slides.add(lastImage);
							}

							// create blank presentation
							pres = new Presentation(slides, "Route slideshow",
									"", 800, 600);

							System.out.println((pres.getSlides().toString()));

							// make slideshow and play
							alss = new SlideShow(pres);

							alss.setOnPause(new EventHandler<SlideShowEvent>() {
								@Override
								public void handle(SlideShowEvent arg0) {
									refUni.resetBrowser();
									ref.removeRoute();
									System.out.println("closed");
									clearTextFields();
								}
							});

							alss.play();

							// assign to stage
							refUni.runRoute(alss);

						}

						Loading.hideBusy();

					}
				};
				new Thread(task).start();

			}
		});
		return go;
	}

	/**
	 * This method clears the text fields in the routeFinder Pane if a route
	 * slide show is closed
	 */
	public static void clearTextFields() {
		if (routeSlideShowPlayed == true) {
			startSearch.setText("");
			finishSearch.setText("");
			routeSlideShowPlayed = false;
		}
	}

	public void createRouteSlideshow() {

		if ((startSearch.getText().isEmpty())
				|| (finishSearch.getText().isEmpty())) {
			// do nothing
		} else {

			System.out.println("start = " + startSearch.getText()
					+ " finish = " + finishSearch.getText());
			// clear slides to make ready for new presentation

			p = 0;
			o = 0;
			m = 0;
			n = 0;

			exroute = new Dijkstras();

			System.out.println("searching start....");

			outerloop: for (i = 0; i <= pois.size() - 1; i++) {
				if (startSearch.getText().equalsIgnoreCase(
						pois.get(i).getName())) {

					startLat = pois.get(i).getLat();
					startLng = pois.get(i).getLng();
					break outerloop;
				}
				for (j = 0; j <= pois.get(i).getPOIList().size() - 1; j++) {
					if (startSearch.getText().equalsIgnoreCase(
							pois.get(i).getPOIList().get(j).getName())) {

						// poiFilename =
						// pois.get(i).getPOIList().get(j).getPhoto().getFilename();
						startLat = pois.get(i).getLat();
						startLng = pois.get(i).getLng();
						break outerloop;

					}
					for (k = 0; k <= pois.get(i).getPOIList().get(j)
							.getFloors().size() - 1; k++) {
						for (l = 0; l <= pois.get(i).getPOIList().get(j)
								.getFloors().get(k).getRooms().size() - 1; l++) {

							if (startSearch.getText()
									.equalsIgnoreCase(
											pois.get(i).getPOIList().get(j)
											.getFloors().get(k)
											.getRooms().get(l)
											.getName())) {

								startNode = pois.get(i).getPOIList().get(j)
										.getFloors().get(k).getRooms().get(l)
										.getNodeNo();

								startLat = pois.get(i).getLat();
								startLng = pois.get(i).getLng();

								System.out.println("startLat =" + startLat);
								System.out.println("startLng =" + startLng);

								break outerloop;
							}
						}
					}
				}
			}

			System.out.println("searching end....");
			// End node choice
			outerloop: for (m = 0; m <= pois.size() - 1; m++) {
				if (finishSearch.getText().equalsIgnoreCase(
						pois.get(m).getName())) {

					endLat = pois.get(m).getLat();
					endLng = pois.get(m).getLng();
					break outerloop;
				}
				for (n = 0; n <= pois.get(m).getPOIList().size() - 1; n++) {
					if (finishSearch.getText().equalsIgnoreCase(
							pois.get(m).getPOIList().get(n).getName())) {

						endLat = pois.get(m).getLat();
						endLng = pois.get(m).getLng();
						break outerloop;

					}
					for (o = 0; o <= pois.get(m).getPOIList().get(n)
							.getFloors().size() - 1; o++) {
						for (p = 0; p <= pois.get(m).getPOIList().get(n)
								.getFloors().get(o).getRooms().size() - 1; p++) {

							if (finishSearch.getText()
									.equalsIgnoreCase(
											pois.get(m).getPOIList().get(n)
											.getFloors().get(o)
											.getRooms().get(p)
											.getName())) {
								finalNode = pois.get(m).getPOIList().get(n)
										.getFloors().get(o).getRooms().get(p)
										.getNodeNo();

								// roomFilename=
								// pois.get(m).getPOIList().get(n).getFloors().get(o).getRooms().get(p).getPhoto().getFilename();
								endLat = pois.get(m).getLat();
								endLng = pois.get(m).getLng();

								// System.out.println("roomphoto = "+
								// roomFilename);
								System.out.println("roomnode = " + finalNode);
								System.out.println("endLat =" + endLat);
								System.out.println("endLng =" + endLng);
								System.out.println("10");
								break outerloop;
							}

						}
					}
				}
			}

			System.out.println("11");

			// if(!poiFilename.isEmpty())
			// add poi image
			firstImage = new Slide("Photo of " + pois.get(m).getName(), 0, 800,
					600);
			System.out.println("1");
			POIImage = new Image(pois.get(m).getPhoto()); // poiFilename
			System.out.println("2");
			POIImageView = new ImageView(POIImage);
			System.out.println("3");
			POIImageView.setPreserveRatio(true);
			System.out.println("4");
			POIImageView.setFitWidth(800);
			System.out.println("5");
			firstImage.setStyle("-fx-background-color: #" + "FFFFFF");
			System.out.println("6");
			firstImage.getChildren().add(POIImageView);
			System.out.println("7");

			System.out.println("POIimage made");

			floorRoutePane = new Slide("Floor Plan", 0, 800, 600);

			// int finalNode = 5;
			if (finalNode != 0) {
				Vertex to = new Vertex("to");
				Vertex from = new Vertex("from");
				// This will always be the start location
				to = exroute.getVertex(finalNode);

				// Do the a certain floorplan route depending on the node
				// number.
				// Each Floorplan has a different starting node number
				// checks to see if startNode is on e
				if (finalNode >= 1 && finalNode <= 24) {
					if (startNode >= 1 && startNode <= 24) {
						from = exroute.getVertex(startNode);
					} else {
						from = exroute.getVertex(1);
					}
					floorRoutePane = exroute.createOfficeRoute(to, from);
				} else if (finalNode >= 43 && finalNode <= 50) {
					if (startNode >= 43 && startNode <= 50) {
						from = exroute.getVertex(startNode);
					} else {
						from = exroute.getVertex(43);
					}
					floorRoutePane = exroute.createElecConRoute(to, from);
				} else if (finalNode >= 25 && finalNode <= 31) {
					if (startNode >= 25 && startNode <= 31) {
						from = exroute.getVertex(startNode);
					} else {
						from = exroute.getVertex(25);
					}
					floorRoutePane = exroute.createElec1Route(to, from);
				} else if (finalNode >= 32 && finalNode <= 38) {
					if (startNode >= 32 && startNode <= 38) {
						from = exroute.getVertex(startNode);
					} else {
						from = exroute.getVertex(32);
					}
					floorRoutePane = exroute.createElec2Route(to, from);
				} else if (finalNode >= 39 && finalNode <= 40) {
					if (startNode >= 39 && startNode <= 40) {
						from = exroute.getVertex(startNode);
					} else {
						from = exroute.getVertex(39);
					}
					floorRoutePane = exroute.createElec3Route(to, from);
				} else if (finalNode >= 41 && finalNode <= 42) {
					if (startNode >= 41 && startNode <= 42) {
						from = exroute.getVertex(startNode);
					} else {
						from = exroute.getVertex(41);
					}
					floorRoutePane = exroute.createElec4Route(to, from);
				}

				if (pois.get(m).getPOIList().get(n).getFloors().get(o)
						.getRooms().get(p).getPhoto().getFilename() != null) {
					// add room image
					lastImage = new Slide("Photo of corridor", 0, 800, 600);

					System.out.println(pois.get(m).getPOIList().get(n)
							.getFloors().get(o).getRooms().get(p).getName()
							.toString());

					Image roomImage = new Image(pois.get(m).getPOIList().get(n)
							.getFloors().get(o).getRooms().get(p).getPhoto()
							.getFilename());// roomFilename
					ImageView roomImageView = new ImageView(roomImage);
					roomImageView.setPreserveRatio(true);

					// If the image is portrait, fit to height
					if (roomImage.getHeight() >= roomImage.getWidth()) {
						roomImageView.setFitHeight(600);
						roomImageView.setTranslateX(400 - ((roomImageView
								.getBoundsInLocal().getWidth()) / 2));
					} else if (roomImage.getHeight() < roomImage.getWidth()) { // If
						// landscape
						// fit
						// to
						// width
						roomImageView.setFitWidth(800);
					}

					lastImage.setStyle("-fx-background-color: #" + "FFFFFF");
					lastImage.getChildren().add(roomImageView);

				}

			}

		}
	}

	/**
	 * This method updates the list after each key press in the textField.
	 */
	public void handleSearchByKey(String oldVal, String newVal) {
		// If the number of characters in the text box is less than last time
		// it must be because the user pressed delete
		if (oldVal != null && (newVal.length() < oldVal.length())) {
			// Restore the lists original set of entries
			// and start from the beginning
			startList.setItems(entries);
		}

		// Change to upper case so that case is not an issue
		newVal = newVal.toUpperCase();

		// Filter out the entries that don't contain the entered text
		ObservableList<String> subentries = FXCollections.observableArrayList();
		for (Object entry : startList.getItems()) {
			String entryText = (String) entry;
			if (entryText.toUpperCase().contains(newVal)) {
				subentries.add(entryText);
			}
		}
		startList.setItems(subentries);

	}

	/**
	 * Method to filter the key presses. Also changes the background of the
	 * textField box to green when the input search matches an item in the start
	 * list.
	 */
	public void handleSearchByKey2(String oldVal, String newVal) {
		// If the number of characters in the text box is less than last time
		// it must be because the user pressed delete
		if (oldVal != null && (newVal.length() < oldVal.length())) {
			// Restore the lists original set of entries
			// and start from the beginning
			startList.setItems(entries);
		}

		// Break out all of the parts of the search text
		// by splitting on white space
		String[] parts = newVal.toUpperCase().split(" ");

		// Filter out the entries that don't contain the entered text
		ObservableList<String> subentries = FXCollections.observableArrayList();
		for (Object entry : startList.getItems()) {
			boolean match = true;
			String entryText = (String) entry;

			for (String part : parts) {
				// The entry needs to contain all portions of the
				// search string *but* in any order
				if (!entryText.toUpperCase().contains(part)) {
					match = false;
					break;
				}
			}

			if (match) {
				subentries.add(entryText);
			}

			// If the search value matches a value in the list highlight box
			// green to show match
			if (newVal.toLowerCase().equals(entryText.toLowerCase())) {
				startSearch.setStyle("-fx-background-color: lightgreen;");
			}
		}
		startList.setItems(subentries);

	}

	/**
	 * Method to filter the key presses. Also changes the background of the
	 * textField box to green when the input search matches an item in the
	 * finish list.
	 */
	public void handleSearchByKey3(String oldVal, String newVal) {
		// If the number of characters in the text box is less than last time
		// it must be because the user pressed delete
		if (oldVal != null && (newVal.length() < oldVal.length())) {
			// Restore the lists original set of entries
			// and start from the beginning
			finishList.setItems(entries);
		}

		// Break out all of the parts of the search text
		// by splitting on white space
		String[] parts = newVal.toUpperCase().split(" ");

		// Filter out the entries that don't contain the entered text
		ObservableList<String> subentries = FXCollections.observableArrayList();
		for (Object entry : finishList.getItems()) {
			boolean match = true;
			String entryText = (String) entry;

			for (String part : parts) {
				// The entry needs to contain all portions of the
				// search string *but* in any order
				if (!entryText.toUpperCase().contains(part)) {
					match = false;
					break;
				}
			}

			if (match) {
				subentries.add(entryText);
			}

			// If the search value matches a value in the list highlight box
			// green to show match
			if (newVal.toLowerCase().equals(entryText.toLowerCase())) {
				finishSearch.setStyle("-fx-background-color: lightgreen;");
			}
		}
		finishList.setItems(subentries);
	}

	/**
	 * Creates a tab pane for the route finder input. Adds all previous methods.
	 * 
	 * @return the tab pane with the search boxes, list and command button
	 */
	public void makeInterface() {
		// Content for routePane
		go = goButton();
		go.setLayoutX(40);
		go.setLayoutY(250);

		startSearch = startSearch();
		startSearch.setPrefWidth(145);
		startSearch.setLayoutX(0);
		startSearch.setLayoutY(10);
		finishSearch = finishSearch();
		finishSearch.setPrefWidth(145);
		finishSearch.setPrefHeight(200);
		finishSearch.setLayoutX(150);
		finishSearch.setLayoutY(10);

		startList.setPrefSize(145, 200);
		startList.setLayoutX(0);
		startList.setLayoutY(40);

		finishList.setPrefSize(145, 200);
		finishList.setLayoutX(150);
		finishList.setLayoutY(40);

		// basic advert created and placed within the routeFinder pane
		Image advert = new Image(getClass().getResourceAsStream("carls.png"));
		final ImageView advertImage = new ImageView(advert);
		advertPane.getChildren().add(advertImage);
		advertPane.setLayoutX(0);
		advertPane.setLayoutY(320);

		// iterate over head POIs
		Iterator<HeadPOI> headPOIItr = pois.iterator();
		while (headPOIItr.hasNext()) {
			HeadPOI headPOI = headPOIItr.next();
			entries.add(headPOI.getName());

			// iterate over POIs
			Iterator<PointOfInterest> POIItr = headPOI.getPOIList().iterator();
			while (POIItr.hasNext()) {
				PointOfInterest poi = POIItr.next();
				entries.add(poi.getName());

				// iterate over floors, but are not added to list
				Iterator<Floor> floorItr = poi.getFloors().iterator();
				while (floorItr.hasNext()) {
					Floor floor = floorItr.next();

					// iterate over rooms
					Iterator<Room> roomItr = floor.getRooms().iterator();
					while (roomItr.hasNext()) {
						Room room = roomItr.next();
						if (room.getName().isEmpty()) {
							entries.add(room.getId());
						} else {
							entries.add(room.getName());
						}
					}
				}
			}
		}
		// Adds the contents of the xml to the start
		// and finish lists
		startList.setItems(entries);
		finishList.setItems(entries);

		getChildren().addAll(go, startList, finishList, startSearch,
				finishSearch, advertPane);
	}

}
