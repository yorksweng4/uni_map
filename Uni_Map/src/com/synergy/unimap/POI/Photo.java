package com.synergy.unimap.POI;

/**
 * holds photo details
 * 
 * @author Rob
 *
 */

public class Photo {

	private String ID;
	private String filename;

	/**
	 *construct photo data structure
	 *
	 * @param ID, photo filename
	 */
	public Photo(String ID, String filename) {
		this.ID = ID;
		this.filename = filename;
	}

	/**
	 *Set photo URL
	 *
	 * @param photo URL
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	/**
	 *get ID
	 *
	 * @return ID
	 */
	public String getID() {
		return this.ID;
	}

	/**
	 *get photo URL
	 *
	 * @return photo URL
	 */
	public String getFilename() {
		return this.filename;
	}

}
