/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.unimap.POI;

import java.util.List;

/**
 * Container for POIs
 * 
 * Combination of Rob's and Kat's POI classes
 * 
 * @author Rob, Kat Young
 *
 */

public class PointOfInterest {

	private String ID;
	private String name;
	private Photo photo;
	private List<Floor> floors;
	
	public PointOfInterest(String ID, String name) {
		this.ID = ID;
		this.name = name;
	}

	/**
	 *Set POI photo
	 *
	 * @return Photo - set photo
	 */
	public void setPhoto(Photo photo) {
		this.photo = photo;
	}

	/**
	 *Set list of floors
	 *
	 * @param floors - Array list of floors
	 */
	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	/**
	 *Get POI name
	 *
	 * @return name - POI name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 *Get POI ID
	 *
	 * @return ID - POI ID
	 */
	public String getID() {
		return this.ID;
	}

	/**
	 *Get POI photo
	 *
	 * @return photo - POI photo
	 */
	public Photo getPhoto() {
		return this.photo;
	}

	/**
	 *Get POI floors
	 *
	 * @return floors - Array list of floors
	 */
	public List<Floor> getFloors() {
		return this.floors;
	}
	
	/**
	 * @return int numberOfFloors
	 */
    public int getNoOfFloors() {
        return floors.size()-1;
    }

}
