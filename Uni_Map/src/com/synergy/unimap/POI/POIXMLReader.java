/*
 * Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
 */
package com.synergy.unimap.POI;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.synergy.unimap.Loading;

/**
 * hacked parser for functionality and testing data structures, based on POI xml
 * file format.
 * 
 * @author Rob, Matt, Kat, Ben

 */

// enum type to track element types within POI xml file
enum ProcessingElement {
	NONE, PHOTO, FLOORPLAN
};

public class POIXMLReader extends DefaultHandler implements Runnable {

	private POIXMLErrors errors;
	private List<PointOfInterest> POIList;
	private PointOfInterest currentPOI;
	private Photo currentPhoto;
	private List<Floor> currentFloorList;
	private Floor currentFloor;
	private Floorplan currentFloorplan;
	private List<Room> currentRoomList;
	private Room currentRoom;
	private ProcessingElement currentElement;

	private ArrayList<HeadPOI> HeadPOIList;
	private HeadPOI currentHeadPOI;

	private Locator loc1;

	private String filename;

	private double lineTotal;

	/**
	 * Constructor
	 * 
	 * @param filename
	 *            the file to parse, including location
	 */
	public POIXMLReader(String filename) {
		errors = new POIXMLErrors();
		currentElement = ProcessingElement.NONE;
		this.filename = filename;

		//Get total lines
		try {
			InputStream is = new URL(filename.replace(" ", "%20")).openStream();
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			lineTotal =  (count == 0 && !empty) ? 1 : count;
			is.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * XML reading process
	 * 
	 * @param filename
	 */
	public void readXMLFile(String filename) {

		try {
			// use the default parser
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			// parse the input
			saxParser.parse(filename, this);
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (SAXException saxe) {
			saxe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * set locator to this
	 * 
	 * @param locator
	 */
	public void setDocumentLocator(Locator l){
		this.loc1 = l;
	}

	/**
	 * Get current line being parsed
	 * 
	 * @return line number
	 */
	public int getLine(){
		return loc1.getLineNumber();
	}

	/**
	 * Called by the parser when it encounters the start of the XML file.
	 */
	public void startDocument() throws SAXException {
		System.out.println("Starting to process POI XML document.");
	}

	/**
	 * Called by parser when it finds a start element
	 */
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		Loading.updateProgress(getLine()/lineTotal);

		String elementName = localName;
		if ("".equals(elementName)) {
			elementName = qName;
		}

		System.out.println("\tFound the start of an element (" + elementName
				+ ") ...");

		// see below for relevant start element methods
		if (elementName.equals("Pois")) {
			// Head POI List
			populateHeadPOIList();

		} else if (elementName.equals("HeadPOI")) {
			// POI
			populatePOIList();
			populateHeadPOI(attributes);

		} else if (elementName.equals("PanoPhoto")) { 
			// HeadPOI PanoPhoto
			String loc = "http://benshouse.crabdance.com/xml/photos/imagenotfound.png";

			//Where possible overwrite the default values
			for(int i = 0; i < attributes.getLength(); i++){
				if(attributes.getLocalName(i).equalsIgnoreCase("location")){
					if(!attributes.getValue(i).isEmpty()){
						loc = attributes.getValue(i);
					}
				}
			}
			currentHeadPOI.setPanoLoc(loc);

		} else if (elementName.equals("Poi")) {
			// POI
			populatePOI(attributes);
		} else if (elementName.equals("PoiPhoto")) {
			//Unused in current XML standards.
			populatePOIPhoto(attributes);
		} else if (elementName.equals("Floors")) {
			// Floor List
			populateFloors(attributes);
		} else if (elementName.equals("Floor")) {
			// Floor
			populateFloor(attributes);
		} else if (elementName.equals("Floorplan")) {
			// Floorplan
			populateFloorplan(attributes);
		} else if (elementName.equals("Rooms")) {
			// Room List
			populateRooms(attributes);
		} else if (elementName.equals("Room")) {
			// Room
			populateRoom(attributes);
		} else if (elementName.equals("RoomPhoto")) {
			// Room Photo
			populateRoomPhoto(attributes);
		} else {
			System.out.println(errors.getError("NullElement"));
		}
	}

	/**
	 * called by parser when characters found. only applies to poi photo, room
	 * photo and floorplan.
	 */
	public void characters(char[] ch, int start, int length)
			throws SAXException {

		Loading.updateProgress(getLine()/lineTotal);

		System.out.println("\tFound some characters.");

		switch (currentElement) {
		case PHOTO:
			currentPhoto.setFilename(new String(ch, start, length));
			break;
		case FLOORPLAN:
			currentFloorplan.setFilename(new String(ch, start, length));
			break;
		default:
			break;
		}
	}

	/**
	 * called by parser when end element found
	 */
	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		Loading.updateProgress(getLine()/lineTotal);

		String elementName = localName;
		if ("".equals(elementName)) {
			elementName = qName;
		}
		System.out.println("\tFound the end of an element (" + elementName
				+ ") ...");

		if (elementName.equals("Pois")) {
			// POI List
			// if end of POI list, do nothing, EOF

		} else if (elementName.equals("HeadPOI")) {
			// POI
			currentHeadPOI.setPOIList(POIList);
			HeadPOIList.add(currentHeadPOI);

			currentHeadPOI = null;
			POIList = null;

		} else if (elementName.equals("PanoPhoto")) {
			// POI PanoPhoto	

		} else if (elementName.equals("Poi")) {
			// POI
			POIList.add(currentPOI);
			currentPOI = null;
		} else if (elementName.equals("PoiPhoto")) {
			// POI Photo
			currentPOI.setPhoto(currentPhoto);
			currentPhoto = null;
			currentElement = ProcessingElement.NONE;
		} else if (elementName.equals("Floors")) {
			// Floor List
			currentPOI.setFloors(currentFloorList);
			currentFloorList = null;
		} else if (elementName.equals("Floor")) {
			// Floor
			currentFloorList.add(currentFloor);
			currentFloor = null;
		} else if (elementName.equals("Floorplan")) {
			// Floorplan
			currentFloor.setFloorplan(currentFloorplan);
			currentFloorplan = null;
			currentElement = ProcessingElement.NONE;
		} else if (elementName.equals("Rooms")) {
			// Room List
			currentFloor.setRooms(currentRoomList);
			currentRoomList = null;
		} else if (elementName.equals("Room")) {
			// Room
			currentRoomList.add(currentRoom);
			currentRoom = null;
		} else if (elementName.equals("RoomPhoto")) {
			// Room Photo
			currentRoom.setPhoto(currentPhoto);
			currentPhoto = null;
			currentElement = ProcessingElement.NONE;
		} else {
			System.out.println(errors.getError("NullElement"));
		}
	}

	/**
	 * called by parser when end of document
	 */
	public void endDocument() throws SAXException {
		System.out.println("Finished processing document.");
	}

	/**
	 * start element functions check attributes are correct length check if
	 * list does not already exist
	 * 
	 * @param attributes
	 */
	private void populateRoomPhoto(Attributes attributes) {
		if (attributes.getLength() == 2) {
			//Set default values
			String id = "Test_photo", filename = "http://benshouse.crabdance.com/xml/photos/imagenotfound.png";

			//Where possible overwrite the default values
			for(int i = 0; i < attributes.getLength(); i++){
				if(attributes.getLocalName(i).equalsIgnoreCase("Id")){
					if(!attributes.getValue(i).isEmpty()){
						id = attributes.getValue(i);
					}
				}
				if(attributes.getLocalName(i).equalsIgnoreCase("Filename")){
					if(!attributes.getValue(i).isEmpty()){
						filename = attributes.getValue(i);
					}
				}
			}

			currentPhoto = new Photo(id, filename);
			currentElement = ProcessingElement.PHOTO;
		} else {
			System.out.println(errors.getError("PhotoAttr"));
		}
	}

	private void populateRoom(Attributes attributes) {
		//Set up default values
		String name = "Default room name", id = "Default room ID", node = "0";

		//Where possible overwrite the default values
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("Name")){
				if(!attributes.getValue(i).isEmpty()){
					name = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("Id")){
				if(!attributes.getValue(i).isEmpty()){
					id = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("NodeNo")){
				if(!attributes.getValue(i).isEmpty()){
					node = attributes.getValue(i);
				}
			}
		}

		currentRoom = new Room(id, name, Integer.parseInt(node));
	}

	private void populateRooms(Attributes attributes) {
		if (attributes.getLength() == 1) {
			if (currentRoomList == null) {
				currentRoomList = new ArrayList<Room>();
			} else {
				System.out.println(errors.getError("RoomsExist"));
			}
		} else {
			System.out.println(errors.getError("RoomsAttr"));
		}
	}

	private void populateFloorplan(Attributes attributes) {
		//Set up default values
		String id = "DefaultID", filename = "http://benshouse.crabdance.com/xml/photos/imagenotfound.png";

		//Where possible overwrite the default values
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("Filename")){
				if(!attributes.getValue(i).isEmpty()){
					filename = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("Id")){
				if(!attributes.getValue(i).isEmpty()){
					id = attributes.getValue(i);
				}
			}
		}

		currentFloorplan = new Floorplan(id, filename);
		currentElement = ProcessingElement.FLOORPLAN;
	}

	private void populateFloor(Attributes attributes) {
		//Set up defaults
		String id = "DefaultID", name = "Default Floor";

		//Where possible overwrite the default values
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("Name")){
				if(!attributes.getValue(i).isEmpty()){
					name = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("Id")){
				if(!attributes.getValue(i).isEmpty()){
					id = attributes.getValue(i);
				}
			}
		}		
		currentFloor = new Floor(id, name);
	}

	private void populateFloors(Attributes attributes) {

		if (attributes.getLength() == 1) {
			if (currentFloorList == null) {
				currentFloorList = new ArrayList<Floor>();
			} else {
				System.out.println(errors.getError("FloorsExist"));
			}
		} else {
			System.out.println(errors.getError("FloorsAttr"));
		}
	}

	//Unused in current XML standards.
	private void populatePOIPhoto(Attributes attributes) {
		//Defaults
		String id = "DefaultPhotoId", filename = "http://benshouse.crabdance.com/xml/photos/imagenotfound.png";

		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("ID")){
				if(!attributes.getValue(i).isEmpty()){
					id = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("filename")){
				if(!attributes.getValue(i).isEmpty()){
					filename = attributes.getValue(i);
				}
			}
		}

		currentPhoto = new Photo(id, filename);
		currentElement = ProcessingElement.PHOTO;

	}

	private void populatePOI(Attributes attributes) {
		//Defaults
		String id = "DefaultPOIid", name = "DefaultPOIname";

		//Scan through attributes. If a relevant one exists and it's not empty, overwrite the default.
		for(int i = 0; i < attributes.getLength(); i++){
			if(attributes.getLocalName(i).equalsIgnoreCase("Id")){
				if(!attributes.getValue(i).isEmpty()){
					id = attributes.getValue(i);
				}
			}
			if(attributes.getLocalName(i).equalsIgnoreCase("Name")){
				if(!attributes.getValue(i).isEmpty()){
					name = attributes.getValue(i);
				}
			}
		}

		currentPOI = new PointOfInterest(id,name);

	}

	private void populateHeadPOI(Attributes attributes){
		if(currentHeadPOI == null) {
			currentHeadPOI = new HeadPOI(attributes);

		} else {
			System.out.println(errors.getError("HeadPOIsExist"));
		}
	}

	private void populatePOIList() {
		if (POIList == null) {
			POIList = new ArrayList<PointOfInterest>();
		} else {
			System.out.println(errors.getError("POIsExist"));
		}
	}

	private void populateHeadPOIList() {
		if (HeadPOIList == null) {
			HeadPOIList = new ArrayList<HeadPOI>();
		} else {
			System.out.println(errors.getError("POIsExist"));
		}
	}

	/**
	 * get POI list
	 * 
	 * @return this.POIList
	 */
	public List<PointOfInterest> getPOIList() {
		return this.POIList;
	}
	public List<HeadPOI> getHeadPOIList() {
		return this.HeadPOIList;
	}

	@Override
	public void run() {
		readXMLFile(filename);

	}

}
