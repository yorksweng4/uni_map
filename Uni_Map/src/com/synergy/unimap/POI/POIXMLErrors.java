/*
* Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
*/
package com.synergy.unimap.POI;

import java.util.HashMap;
import java.util.Map;

/**
 * holds error list for POIXMLReader
 * makes use of a hash table to store errors
 * 
 * @author Rob
 *
 */

public class POIXMLErrors {
	
	private Map<String, String> errors;

	POIXMLErrors() {
		errors = new HashMap<String, String>();
		populateMap();
	}
	
	/**
	 * set relevant error messages
	 */
	private void populateMap() {
		errors.put("RoomAttr", "Current Room does not contain valid attributes.");
		errors.put("RoomsExist", "Current Room list already exists.");
		errors.put("RoomsAttr", "Current Room list does not contain valid attributes.");
		errors.put("FloorplanAttr", "Current floorplan does not contain valid attributes.");
		errors.put("FloorAttr", "Current floor does not contain valid attributes.");
		errors.put("FloorsExist", "An instance of Floor list already exists.");
		errors.put("FloorsAttr", "Current Floor list contains incorrect attributes.");
		errors.put("PhotoAttr", "Current POI Photo does not contain valid attributes.");
		errors.put("IconAttr", "Current Head POI Icon does not contain valid attributes.");
		errors.put("POIAttr", "Current POI does not contain valid attributes.");
		errors.put("POIsExist", " An instance of POI List already exists.");
		errors.put("NullElement", "Current element does not have valid type.");
	}

	/**
	 * get relevant error messages
	 * @return error
	 */
	public String getError(String error) {
		return String.format("ERROR: %s", errors.get(error));
	}
}
