/*
* Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
*/
package com.synergy.unimap.POI;

/** 
 * This is the result of integrating Rob's and Kat's Room classes
 * 
 * @author Kat Young, Rob
 */

public class Room {

	private String id;
	private String name;
	private Photo photo;
	private int nodeNo;
	
	/**
	 * constructor gives each room an id and a name
	 */
    public Room(String id, String name) {
    	this.id = new String(id);
    	this.name = new String(name);
    }
    
	/**
	 * constructor gives each room an id if no name
	 */
    public Room(String id) {
    	this.id = new String(id);
    	this.name = new String();
    }
	
    // below are the constructors for when a node no and coords are used
    
	/**
	 * constructor gives each room an id, a name, a node number
	 */
    public Room(String id, String name, int nodeNo) {
    	this.id = new String(id);
    	this.name = new String(name);
    	this.nodeNo = nodeNo;

    }
    
	/**
	 * constructor for node number if XML only has id
	 */
    public Room(String id, int nodeNo) {
    	this.id = new String(id);
    	this.name = new String();
    	this.nodeNo = nodeNo;

    }
    
	/**
	 * @return String id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return String id
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param String name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return photo
	 */
	public Photo getPhoto() {
		return this.photo;
	}
	
	/**
	 * @param photo
	 */
	public void setPhoto(Photo photo) {
		this.photo = photo;
	}

	public int getNodeNo() {
		return nodeNo;
	}

	public void setNodeNo(int nodeNo) {
		this.nodeNo = nodeNo;
	}
}
