package com.synergy.unimap.POI;

import java.util.Iterator;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import com.synergy.unimap.MapView;

/**
 * creates a pane to display the tree of Head POI, POI, Floors and Room on the
 * GUI tree with a text search to filter the displayed tree. When a node is
 * clicked, the webmap centres on the appropriate head POI
 * 
 * @author Kat
 */

public class POIInfoPane extends Pane {

	// rootNode to hold tree together
	private TreeItem<String> rootNode;

	// TreeView to be able to view treeItems
	private TreeView<String> treeView;

	// Vbox to place information items vertically
	final VBox infoVPane = new VBox();

	// GridPane to hold 'tab' buttons
	final GridPane buttonPane = new GridPane();

	// search bar
	final TextField search = new TextField();

	private MapView browser;

	//
	private List<HeadPOI> pois;

	public POIInfoPane(List<HeadPOI> list, MapView ref) {
		super();

		browser = ref;

		// take pois from parser, all upper level
		this.pois = list;

		// making everything look pretty and spaced nicely
		infoVPane.setSpacing(5);
		infoVPane.setPadding(new Insets(3));

		buttonPane.setHgap(5);
		buttonPane.setAlignment(Pos.CENTER);

		search.setPromptText("Search");
		search.textProperty().addListener(new ChangeListener<Object>() {

			@Override
			public void changed(ObservableValue<?> arg0, Object arg1,
					Object arg2) {
				refreshTree(search.getText());
			}
		});

		// adding items to Vbox in order top to bottom
		infoVPane.getChildren().add(buttonPane);
		infoVPane.getChildren().add(search);

		// simulate a search for "" to make sure everything is displayed
		// contains adding tree to infoVPane
		refreshTree("");

		// starting the cursor in the search text box
		search.requestFocus();

		this.getChildren().add(infoVPane);
	}

	/**
	 * Filters the POI tree in the GUI based on the content on the search text
	 * box. Iterates through POI, Floors and Rooms to find rooms that match the
	 * search contents, then adds room to floor, adds floors if not present
	 * already, adds POI if not present already
	 * 
	 * @param String
	 *            search - whatever the user is searching for
	 */
	private void refreshTree(String search) {
		rootNode = new TreeItem<String>("University of York");

		// iterate through headPOI list
		Iterator<HeadPOI> headPOIItr = pois.iterator();

		while (headPOIItr.hasNext()) {
			HeadPOI headPOI = headPOIItr.next();

			TreeItem<String> headPOINode = new TreeItem<String>(
					headPOI.getName());

			// if the poi matches search criteria, add poi to tree
			if (headPOI.getName().toLowerCase().contains(search.toLowerCase())) {
				rootNode.getChildren().add(headPOINode);
			}

			// iterate through poi list
			Iterator<PointOfInterest> POIItr = headPOI.getPOIList().iterator();

			while (POIItr.hasNext()) {
				PointOfInterest pointOfInterest = POIItr.next();

				// create poi node based on current poi
				TreeItem<String> poiNode = new TreeItem<String>(
						pointOfInterest.getName());

				// if the poi matches search criteria, add poi to tree
				if (pointOfInterest.getName().toLowerCase()
						.contains(search.toLowerCase())) {
					headPOINode.getChildren().add(poiNode);
				}

				// iterate over floors in current poi
				Iterator<Floor> floorItr = pointOfInterest.getFloors()
						.iterator();

				while (floorItr.hasNext()) {
					Floor floor = floorItr.next();

					// create floor node based on current floor
					TreeItem<String> floorNode = new TreeItem<String>(
							floor.getName());

					// iterate over rooms on current floor
					Iterator<Room> roomItr = floor.getRooms().iterator();
					while (roomItr.hasNext()) {
						// get next room
						Room room = roomItr.next();

						// create room node, initialised later
						TreeItem<String> roomNode;

						if (room.getName().isEmpty()) {
							// if no room name, assign room id to room node
							// value
							roomNode = new TreeItem<String>(room.getId());
						} else {
							// else assign room name to room node value
							roomNode = new TreeItem<String>(room.getName());
						}

						// if current room matches search criteria
						if (roomNode.getValue().toLowerCase()
								.contains(search.toLowerCase())) {
							// add current room node to current floor node
							floorNode.getChildren().add(roomNode);

							// if current floor node does not exist in tree
							if (!poiNode.getChildren().contains(floorNode)) {
								// add current floor node to tree
								poiNode.getChildren().add(floorNode);
							}

							// if current poi node does not exist in tree
							if (!headPOINode.getChildren().contains(poiNode)) {
								// add current poi node to tree
								headPOINode.getChildren().add(poiNode);
							}

							// if current poi node does not exist in tree
							if (!rootNode.getChildren().contains(headPOINode)) {
								// add current poi node to tree
								rootNode.getChildren().add(headPOINode);
							}
						}

						// expand root node
						rootNode.setExpanded(true);
					}
				}
			}
		}

		// removing the old treeView and adding the newly built one
		infoVPane.getChildren().remove(treeView);
		treeView = new TreeView<String>(rootNode);

		// adds a Listener to the treeView to listen for click changes, then
		// centers on the relevant head POI
		treeView.getSelectionModel().selectedItemProperty()
				.addListener(new ChangeListener<TreeItem<String>>() {

					@Override
					public void changed(
							ObservableValue<? extends TreeItem<String>> arg0,
							TreeItem<String> arg1, TreeItem<String> selectedItem) {

						// iterate through headPOI list
						Iterator<HeadPOI> headPOIItr = pois.iterator();
						while (headPOIItr.hasNext()) {
							HeadPOI headPOI = headPOIItr.next();

							// if the selected item matches a head POI, centre
							// on that head POI
							if (selectedItem.getValue().equals(
									headPOI.getName())) {
								// Centre map on Head POI
								browser.centerMap(headPOI.getLat(),
										headPOI.getLng(), 17);
							}

							// iterate through POI list
							Iterator<PointOfInterest> POIItr = headPOI
									.getPOIList().iterator();
							while (POIItr.hasNext()) {
								PointOfInterest poi = POIItr.next();

								// if the selected item matches a POI, centre on
								// that POI's head POI
								if (selectedItem.getValue().equals(
										poi.getName())) {
									// Centre map on Head POI
									browser.centerMap(headPOI.getLat(),
											headPOI.getLng(), 17);
								}

								// iterate over floors but do not include in the
								// centering on click
								Iterator<Floor> floorItr = poi.getFloors()
										.iterator();
								while (floorItr.hasNext()) {
									Floor floor = floorItr.next();

									// iterate over rooms
									Iterator<Room> roomItr = floor.getRooms()
											.iterator();
									while (roomItr.hasNext()) {
										Room room = roomItr.next();

										// if the selected item matches a POI,
										// centre on that POI's head POI
										if (selectedItem.getValue().equals(
												room.getName())) {
											// Centre map on Head POI
											browser.centerMap(headPOI.getLat(),
													headPOI.getLng(), 17);
										}
									}
								}
							}
						}

					}
				});

		infoVPane.getChildren().add(treeView);
	}

}
