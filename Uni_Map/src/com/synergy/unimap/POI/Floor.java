/*
* Copyright (c) 2013 Synergy Software Solutions. All rights reserved.
*/
package com.synergy.unimap.POI;
import java.util.ArrayList;
import java.util.List;

/**
 * a combinations of Rob's and Kat's Floor classes
 * 
 * @author Kat Young, Rob
 */

public class Floor {

	//each floor contains an arraylist of rooms
	private String id;
	private String name;
    private ArrayList<Room> roomList;
	private Floorplan floorplan;

	public Floor(String id, String name) {
        this.id = new String(id);
		this.name = new String(name);
        this.roomList = new ArrayList<Room>();
    }    
    
	/**
	 * @return String id
	 */
    public String getId() {
		return id;
	}

	/**
	 * @param String id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return String name
	 */
	public String getName() {
        return name;
    }

	/**
	 * @param String name
	 */
    public void setName(String name) {
        this.name = name;
    }

	/**
	 * @return int numberOfRooms
	 */
    public int getNoOfRooms() {
    	int q = roomList.size()-1;
        return q;
    }

	/**
	 * @param String name
	 * @return ArrayList<Room> room
	 */
	public ArrayList<Room> getRooms() {	
		return roomList;
	}

	/**
	 * adds argument room to the ArrayList of Rooms
	 * 
	 * @param InfoRoom room
	 */
	public void addRooms(Room room) {
		this.roomList.add(room);
	}
	
	/**
	 * @param roomList
	 */
	public void setRooms(List<Room> roomList) {
		this.roomList = (ArrayList<Room>) roomList;
	}
	
	/**
	 * @param floorplan
	 */
	public void setFloorplan(Floorplan floorplan) {
		this.floorplan = floorplan;
	}

	/**
	 * @return this floor's floorplan
	 */
	public Floorplan getFloorplan() {
		return this.floorplan;
	}
}

