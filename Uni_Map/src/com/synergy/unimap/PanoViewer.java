package com.synergy.unimap;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javafx.animation.FadeTransition;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Duration;


/**
 * 
 * This class provides a region in which a panorama image may be viewed.
 * The panorama image can be scrolled horizontally, and the size of the viewed windows is independent
 * of the size of the image.
 * 
 * @author Eddy, Ben
 *
 */
public class PanoViewer extends Pane{

	private double mousePos;
	private InputStream inURL = null;
	private String panoLoc;
	private Image pano = null;
	private Pane viewer;
	
	/**
	 * Constructor.
	 * 
	 * @param pano the image to be viewed
	 * @param width width of the region
	 * @param height height of the region
	 */
	public PanoViewer(String panoL, double width, double height){
		
		viewer = this;
		this.panoLoc = panoL;
		
		// set the size
		setMaxSize(width, height);
		setPrefWidth(width);
		
		System.out.println(getMaxWidth());
		
		Task<Void> task = new Task<Void>() {
			
			 @Override protected void scheduled() {
			        super.succeeded();
			        Loading.showBusy();
			}
			
		    @Override public Void call() {
		    	
		    	try {
					inURL = new BufferedInputStream(new URL(panoLoc).openStream());
					pano = new Image(inURL);
					System.out.println(panoLoc);
					
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		    	
		        return null;
		    }
		    
		    @Override protected void succeeded() {
		        super.succeeded();
		        
		     // create imageview
				final ImageView image = new ImageView(pano);
				image.setPreserveRatio(true);
				image.setFitHeight(600);

				// centre the image on the screen
				image.setTranslateX((- image.getImage().getWidth() + getPrefWidth()) / 2);

				// when mouse pressed, get current position with respect to image
				image.setOnMousePressed(new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent e) {
						mousePos = e.getX();
					}
				});

				// when mouse dragged, translate image to new mouse position, but only if the region is still filled by the image
				image.setOnMouseDragged(new EventHandler<MouseEvent>() {

					@Override
					public void handle(MouseEvent e) {
						double newPos = e.getSceneX() - mousePos;
						if(newPos <= 0 && newPos >= - image.getImage().getWidth() + getMaxWidth()){
							image.setTranslateX(newPos);
						}
					}

				});

				if(!Loading.getKillThread()){
					// add child
					getChildren().add(image);
				}
				
				// exit button
				final Button exitPano;
				exitPano = new Button("X");
				exitPano.setStyle("-fx-base: red;" + "-fx-font-weight: bold;");
				exitPano.setLayoutX(viewer.getPrefWidth() - 40);
				exitPano.setLayoutY(5);
				
				getChildren().add(exitPano);
				
				// action listener, pause() on press
				exitPano.setOnAction(new EventHandler<ActionEvent>() {

					@Override
					public void handle(ActionEvent e) {
						// fade out
						FadeTransition ft = new FadeTransition(Duration.millis(200), viewer);
						ft.setFromValue(1.0);
						ft.setToValue(0.0);

						// invisible when finished
						ft.setOnFinished(new EventHandler<ActionEvent>() {

							@Override
							public void handle(ActionEvent arg0) {
								viewer.setVisible(false);	
							}
						});

						// go!
						ft.play();
					}

				});
		        
				Loading.hideBusy();
				
		    }
		};
		new Thread(task).start();

		
	}

}
